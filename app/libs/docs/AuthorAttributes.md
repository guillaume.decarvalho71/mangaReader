# AuthorAttributes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**imageUrl** | **String** |  |  [optional]
**biography** | [**LocalizedString**](LocalizedString.md) |  |  [optional]
**twitter** | **String** |  |  [optional]
**pixiv** | **String** |  |  [optional]
**melonBook** | **String** |  |  [optional]
**fanBox** | **String** |  |  [optional]
**booth** | **String** |  |  [optional]
**nicoVideo** | **String** |  |  [optional]
**skeb** | **String** |  |  [optional]
**fantia** | **String** |  |  [optional]
**tumblr** | **String** |  |  [optional]
**youtube** | **String** |  |  [optional]
**weibo** | **String** |  |  [optional]
**naver** | **String** |  |  [optional]
**website** | **String** |  |  [optional]
**version** | **Integer** |  |  [optional]
**createdAt** | **String** |  |  [optional]
**updatedAt** | **String** |  |  [optional]
