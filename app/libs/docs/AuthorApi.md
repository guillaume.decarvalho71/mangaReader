# AuthorApi

All URIs are relative to *https://api.mangadex.org*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteAuthorId**](AuthorApi.md#deleteAuthorId) | **DELETE** author/{id} | Delete Author
[**getAuthor**](AuthorApi.md#getAuthor) | **GET** author | Author list
[**getAuthorId**](AuthorApi.md#getAuthorId) | **GET** author/{id} | Get Author
[**postAuthor**](AuthorApi.md#postAuthor) | **POST** author | Create Author
[**putAuthorId**](AuthorApi.md#putAuthorId) | **PUT** author/{id} | Update Author

<a name="deleteAuthorId"></a>
# **deleteAuthorId**
> Response deleteAuthorId(id)

Delete Author

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AuthorApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


AuthorApi apiInstance = new AuthorApi();
UUID id = new UUID(); // UUID | Author ID
try {
    Response result = apiInstance.deleteAuthorId(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthorApi#deleteAuthorId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| Author ID |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAuthor"></a>
# **getAuthor**
> AuthorList getAuthor(limit, offset, ids, name, orderName, includes)

Author list

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.AuthorApi;


AuthorApi apiInstance = new AuthorApi();
Integer limit = 10; // Integer | 
Integer offset = 56; // Integer | 
List<UUID> ids = Arrays.asList(new UUID()); // List<UUID> | Author ids (limited to 100 per request)
String name = "name_example"; // String | 
String orderName = "orderName_example"; // String | 
List<String> includes = Arrays.asList("includes_example"); // List<String> | 
try {
    AuthorList result = apiInstance.getAuthor(limit, offset, ids, name, orderName, includes);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthorApi#getAuthor");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Integer**|  | [optional] [default to 10] [enum: ]
 **offset** | **Integer**|  | [optional] [enum: ]
 **ids** | [**List&lt;UUID&gt;**](UUID.md)| Author ids (limited to 100 per request) | [optional]
 **name** | **String**|  | [optional]
 **orderName** | **String**|  | [optional] [enum: asc, desc]
 **includes** | [**List&lt;String&gt;**](String.md)|  | [optional]

### Return type

[**AuthorList**](AuthorList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAuthorId"></a>
# **getAuthorId**
> AuthorResponse getAuthorId(id, includes)

Get Author

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.AuthorApi;


AuthorApi apiInstance = new AuthorApi();
UUID id = new UUID(); // UUID | Author ID
List<String> includes = Arrays.asList("includes_example"); // List<String> | 
try {
    AuthorResponse result = apiInstance.getAuthorId(id, includes);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthorApi#getAuthorId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| Author ID |
 **includes** | [**List&lt;String&gt;**](String.md)|  | [optional]

### Return type

[**AuthorResponse**](AuthorResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postAuthor"></a>
# **postAuthor**
> AuthorResponse postAuthor(contentType, body)

Create Author

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AuthorApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


AuthorApi apiInstance = new AuthorApi();
String contentType = "application/json"; // String | 
AuthorCreate body = new AuthorCreate(); // AuthorCreate | The size of the body is limited to 8KB.
try {
    AuthorResponse result = apiInstance.postAuthor(contentType, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthorApi#postAuthor");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **body** | [**AuthorCreate**](AuthorCreate.md)| The size of the body is limited to 8KB. | [optional]

### Return type

[**AuthorResponse**](AuthorResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putAuthorId"></a>
# **putAuthorId**
> AuthorResponse putAuthorId(contentType, id, body)

Update Author

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AuthorApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


AuthorApi apiInstance = new AuthorApi();
String contentType = "application/json"; // String | 
UUID id = new UUID(); // UUID | Author ID
AuthorEdit body = new AuthorEdit(); // AuthorEdit | The size of the body is limited to 8KB.
try {
    AuthorResponse result = apiInstance.putAuthorId(contentType, id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthorApi#putAuthorId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **id** | [**UUID**](.md)| Author ID |
 **body** | [**AuthorEdit**](AuthorEdit.md)| The size of the body is limited to 8KB. | [optional]

### Return type

[**AuthorResponse**](AuthorResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

