# InlineResponse2009Data

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**UUID**](UUID.md) |  |  [optional]
**type** | **String** |  |  [optional]
**attributes** | [**InlineResponse2009Attributes**](InlineResponse2009Attributes.md) |  |  [optional]
