# SettingsApi

All URIs are relative to *https://api.mangadex.org*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getSettings**](SettingsApi.md#getSettings) | **GET** settings | Get an User Settings
[**getSettingsTemplate**](SettingsApi.md#getSettingsTemplate) | **GET** settings/template | Get latest Settings template
[**getSettingsTemplateVersion**](SettingsApi.md#getSettingsTemplateVersion) | **GET** settings/template/{version} | Get Settings template by version id
[**postSettings**](SettingsApi.md#postSettings) | **POST** settings | Create or update an User Settings
[**postSettingsTemplate**](SettingsApi.md#postSettingsTemplate) | **POST** settings/template | Create Settings template

<a name="getSettings"></a>
# **getSettings**
> InlineResponse20014 getSettings()

Get an User Settings

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SettingsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


SettingsApi apiInstance = new SettingsApi();
try {
    InlineResponse20014 result = apiInstance.getSettings();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SettingsApi#getSettings");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20014**](InlineResponse20014.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getSettingsTemplate"></a>
# **getSettingsTemplate**
> Object getSettingsTemplate()

Get latest Settings template

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SettingsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


SettingsApi apiInstance = new SettingsApi();
try {
    Object result = apiInstance.getSettingsTemplate();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SettingsApi#getSettingsTemplate");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getSettingsTemplateVersion"></a>
# **getSettingsTemplateVersion**
> Object getSettingsTemplateVersion(version)

Get Settings template by version id

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SettingsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


SettingsApi apiInstance = new SettingsApi();
UUID version = new UUID(); // UUID | 
try {
    Object result = apiInstance.getSettingsTemplateVersion(version);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SettingsApi#getSettingsTemplateVersion");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | [**UUID**](.md)|  |

### Return type

**Object**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postSettings"></a>
# **postSettings**
> InlineResponse20015 postSettings(body)

Create or update an User Settings

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SettingsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


SettingsApi apiInstance = new SettingsApi();
SettingsBody body = new SettingsBody(); // SettingsBody | 
try {
    InlineResponse20015 result = apiInstance.postSettings(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SettingsApi#postSettings");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**SettingsBody**](SettingsBody.md)|  | [optional]

### Return type

[**InlineResponse20015**](InlineResponse20015.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postSettingsTemplate"></a>
# **postSettingsTemplate**
> Object postSettingsTemplate(body)

Create Settings template

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.SettingsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


SettingsApi apiInstance = new SettingsApi();
Object body = null; // Object | 
try {
    Object result = apiInstance.postSettingsTemplate(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SettingsApi#postSettingsTemplate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Object**](Object.md)|  | [optional]

### Return type

**Object**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

