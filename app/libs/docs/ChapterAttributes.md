# ChapterAttributes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** |  |  [optional]
**volume** | **String** |  |  [optional]
**chapter** | **String** |  |  [optional]
**pages** | **Integer** | Count of readable images for this chapter |  [optional]
**translatedLanguage** | **String** |  |  [optional]
**uploader** | [**UUID**](UUID.md) |  |  [optional]
**externalUrl** | **String** | Denotes a chapter that links to an external source. |  [optional]
**version** | **Integer** |  |  [optional]
**createdAt** | **String** |  |  [optional]
**updatedAt** | **String** |  |  [optional]
**publishAt** | **String** |  |  [optional]
**readableAt** | **String** |  |  [optional]
