# StatisticsApi

All URIs are relative to *https://api.mangadex.org*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getStatisticsManga**](StatisticsApi.md#getStatisticsManga) | **GET** statistics/manga | Find statistics about given Manga
[**getStatisticsMangaUuid**](StatisticsApi.md#getStatisticsMangaUuid) | **GET** statistics/manga/{uuid} | Get statistics about given Manga

<a name="getStatisticsManga"></a>
# **getStatisticsManga**
> InlineResponse20013 getStatisticsManga()

Find statistics about given Manga

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StatisticsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


StatisticsApi apiInstance = new StatisticsApi();
try {
    InlineResponse20013 result = apiInstance.getStatisticsManga();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StatisticsApi#getStatisticsManga");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20013**](InlineResponse20013.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getStatisticsMangaUuid"></a>
# **getStatisticsMangaUuid**
> InlineResponse20012 getStatisticsMangaUuid(uuid)

Get statistics about given Manga

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.StatisticsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


StatisticsApi apiInstance = new StatisticsApi();
UUID uuid = new UUID(); // UUID | 
try {
    InlineResponse20012 result = apiInstance.getStatisticsMangaUuid(uuid);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StatisticsApi#getStatisticsMangaUuid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uuid** | [**UUID**](.md)|  |

### Return type

[**InlineResponse20012**](InlineResponse20012.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

