# InlineResponse20013Statistics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rating** | [**InlineResponse20013Rating**](InlineResponse20013Rating.md) |  |  [optional]
**follows** | **Integer** |  |  [optional]
