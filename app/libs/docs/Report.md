# Report

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**UUID**](UUID.md) |  |  [optional]
**type** | [**TypeEnum**](#TypeEnum) |  |  [optional]
**attributes** | [**ReportAttributes**](ReportAttributes.md) |  |  [optional]
**relationships** | [**List&lt;Relationship&gt;**](Relationship.md) |  |  [optional]

<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
REPORT | &quot;report&quot;
