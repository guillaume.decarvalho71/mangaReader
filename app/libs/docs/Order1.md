# Order1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | [**NameEnum**](#NameEnum) |  |  [optional]
**createdAt** | [**CreatedAtEnum**](#CreatedAtEnum) |  |  [optional]
**updatedAt** | [**UpdatedAtEnum**](#UpdatedAtEnum) |  |  [optional]
**followedCount** | [**FollowedCountEnum**](#FollowedCountEnum) |  |  [optional]
**relevance** | [**RelevanceEnum**](#RelevanceEnum) |  |  [optional]

<a name="NameEnum"></a>
## Enum: NameEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="CreatedAtEnum"></a>
## Enum: CreatedAtEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="UpdatedAtEnum"></a>
## Enum: UpdatedAtEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="FollowedCountEnum"></a>
## Enum: FollowedCountEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="RelevanceEnum"></a>
## Enum: RelevanceEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;
