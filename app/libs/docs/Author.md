# Author

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**UUID**](UUID.md) |  |  [optional]
**type** | [**TypeEnum**](#TypeEnum) |  |  [optional]
**attributes** | [**AuthorAttributes**](AuthorAttributes.md) |  |  [optional]
**relationships** | [**List&lt;Relationship&gt;**](Relationship.md) |  |  [optional]

<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
AUTHOR | &quot;author&quot;
