# InlineResponse20011

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | **String** |  |  [optional]
**ratings** | [**Map&lt;String, InlineResponse20011Ratings&gt;**](InlineResponse20011Ratings.md) |  |  [optional]
