# MappingIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**TypeEnum**](#TypeEnum) |  |  [optional]
**ids** | **List&lt;Integer&gt;** |  |  [optional]

<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
GROUP | &quot;group&quot;
MANGA | &quot;manga&quot;
CHAPTER | &quot;chapter&quot;
TAG | &quot;tag&quot;
