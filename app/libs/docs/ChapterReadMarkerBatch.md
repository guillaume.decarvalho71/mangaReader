# ChapterReadMarkerBatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chapterIdsRead** | [**List&lt;UUID&gt;**](UUID.md) |  |  [optional]
**chapterIdsUnread** | [**List&lt;UUID&gt;**](UUID.md) |  |  [optional]
