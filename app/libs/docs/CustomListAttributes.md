# CustomListAttributes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**visibility** | [**VisibilityEnum**](#VisibilityEnum) |  |  [optional]
**version** | **Integer** |  |  [optional]

<a name="VisibilityEnum"></a>
## Enum: VisibilityEnum
Name | Value
---- | -----
PRIVATE | &quot;private&quot;
PUBLIC | &quot;public&quot;
