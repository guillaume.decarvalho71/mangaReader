# Cover

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**UUID**](UUID.md) |  |  [optional]
**type** | [**TypeEnum**](#TypeEnum) |  |  [optional]
**attributes** | [**CoverAttributes**](CoverAttributes.md) |  |  [optional]
**relationships** | [**List&lt;Relationship&gt;**](Relationship.md) |  |  [optional]

<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
COVER_ART | &quot;cover_art&quot;
