# InlineResponse20016

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | **String** |  |  [optional]
**ratings** | [**List&lt;InlineResponse20016Ratings&gt;**](InlineResponse20016Ratings.md) |  |  [optional]
