# ChapterRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** |  |  [optional]
**volume** | **String** |  |  [optional]
**chapter** | **String** |  |  [optional]
**translatedLanguage** | **String** |  |  [optional]
**groups** | [**List&lt;UUID&gt;**](UUID.md) |  |  [optional]
**version** | **Integer** |  |  [optional]
