# MappingIdAttributes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**TypeEnum**](#TypeEnum) |  |  [optional]
**legacyId** | **Integer** |  |  [optional]
**newId** | [**UUID**](UUID.md) |  |  [optional]

<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
MANGA | &quot;manga&quot;
CHAPTER | &quot;chapter&quot;
GROUP | &quot;group&quot;
TAG | &quot;tag&quot;
