# InlineResponse20012

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | **String** |  |  [optional]
**statistics** | [**Map&lt;String, InlineResponse20012Statistics&gt;**](InlineResponse20012Statistics.md) |  |  [optional]
