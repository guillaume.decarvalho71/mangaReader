# InlineResponse2005Chapter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **String** |  |  [optional]
**data** | **List&lt;String&gt;** |  |  [optional]
**dataSaver** | **List&lt;String&gt;** |  |  [optional]
