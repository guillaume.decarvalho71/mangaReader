# AuthApi

All URIs are relative to *https://api.mangadex.org*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAuthCheck**](AuthApi.md#getAuthCheck) | **GET** auth/check | Check token
[**postAuthLogin**](AuthApi.md#postAuthLogin) | **POST** auth/login | Login
[**postAuthLogout**](AuthApi.md#postAuthLogout) | **POST** auth/logout | Logout
[**postAuthRefresh**](AuthApi.md#postAuthRefresh) | **POST** auth/refresh | Refresh token

<a name="getAuthCheck"></a>
# **getAuthCheck**
> CheckResponse getAuthCheck()

Check token

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AuthApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


AuthApi apiInstance = new AuthApi();
try {
    CheckResponse result = apiInstance.getAuthCheck();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthApi#getAuthCheck");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CheckResponse**](CheckResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postAuthLogin"></a>
# **postAuthLogin**
> LoginResponse postAuthLogin(contentType, body)

Login

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.AuthApi;


AuthApi apiInstance = new AuthApi();
String contentType = "application/json"; // String | 
Login body = new Login(); // Login | The size of the body is limited to 2KB.
try {
    LoginResponse result = apiInstance.postAuthLogin(contentType, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthApi#postAuthLogin");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **body** | [**Login**](Login.md)| The size of the body is limited to 2KB. | [optional]

### Return type

[**LoginResponse**](LoginResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postAuthLogout"></a>
# **postAuthLogout**
> LogoutResponse postAuthLogout()

Logout

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.AuthApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


AuthApi apiInstance = new AuthApi();
try {
    LogoutResponse result = apiInstance.postAuthLogout();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthApi#postAuthLogout");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**LogoutResponse**](LogoutResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postAuthRefresh"></a>
# **postAuthRefresh**
> RefreshResponse postAuthRefresh(contentType, body)

Refresh token

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.AuthApi;


AuthApi apiInstance = new AuthApi();
String contentType = "application/json"; // String | 
RefreshToken body = new RefreshToken(); // RefreshToken | The size of the body is limited to 2KB.
try {
    RefreshResponse result = apiInstance.postAuthRefresh(contentType, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthApi#postAuthRefresh");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **body** | [**RefreshToken**](RefreshToken.md)| The size of the body is limited to 2KB. | [optional]

### Return type

[**RefreshResponse**](RefreshResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

