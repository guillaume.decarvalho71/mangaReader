# ScanlationGroupApi

All URIs are relative to *https://api.mangadex.org*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteGroupId**](ScanlationGroupApi.md#deleteGroupId) | **DELETE** group/{id} | Delete Scanlation Group
[**deleteGroupIdFollow**](ScanlationGroupApi.md#deleteGroupIdFollow) | **DELETE** group/{id}/follow | Unfollow Scanlation Group
[**getGroupId**](ScanlationGroupApi.md#getGroupId) | **GET** group/{id} | Get Scanlation Group
[**getSearchGroup**](ScanlationGroupApi.md#getSearchGroup) | **GET** group | Scanlation Group list
[**postGroup**](ScanlationGroupApi.md#postGroup) | **POST** group | Create Scanlation Group
[**postGroupIdFollow**](ScanlationGroupApi.md#postGroupIdFollow) | **POST** group/{id}/follow | Follow Scanlation Group
[**putGroupId**](ScanlationGroupApi.md#putGroupId) | **PUT** group/{id} | Update Scanlation Group

<a name="deleteGroupId"></a>
# **deleteGroupId**
> Response deleteGroupId(id)

Delete Scanlation Group

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ScanlationGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


ScanlationGroupApi apiInstance = new ScanlationGroupApi();
UUID id = new UUID(); // UUID | Scanlation Group ID
try {
    Response result = apiInstance.deleteGroupId(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ScanlationGroupApi#deleteGroupId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| Scanlation Group ID |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteGroupIdFollow"></a>
# **deleteGroupIdFollow**
> Response deleteGroupIdFollow(id)

Unfollow Scanlation Group

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ScanlationGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


ScanlationGroupApi apiInstance = new ScanlationGroupApi();
UUID id = new UUID(); // UUID | 
try {
    Response result = apiInstance.deleteGroupIdFollow(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ScanlationGroupApi#deleteGroupIdFollow");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)|  |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getGroupId"></a>
# **getGroupId**
> ScanlationGroupResponse getGroupId(id, includes)

Get Scanlation Group

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ScanlationGroupApi;


ScanlationGroupApi apiInstance = new ScanlationGroupApi();
UUID id = new UUID(); // UUID | Scanlation Group ID
List<String> includes = Arrays.asList("includes_example"); // List<String> | 
try {
    ScanlationGroupResponse result = apiInstance.getGroupId(id, includes);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ScanlationGroupApi#getGroupId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| Scanlation Group ID |
 **includes** | [**List&lt;String&gt;**](String.md)|  | [optional]

### Return type

[**ScanlationGroupResponse**](ScanlationGroupResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getSearchGroup"></a>
# **getSearchGroup**
> ScanlationGroupList getSearchGroup(limit, offset, ids, name, focusedLanguage, includes, orderName, orderCreatedAt, orderUpdatedAt, orderFollowedCount, orderRelevance)

Scanlation Group list

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ScanlationGroupApi;


ScanlationGroupApi apiInstance = new ScanlationGroupApi();
Integer limit = 10; // Integer | 
Integer offset = 56; // Integer | 
List<UUID> ids = Arrays.asList(new UUID()); // List<UUID> | ScanlationGroup ids (limited to 100 per request)
String name = "name_example"; // String | 
String focusedLanguage = "focusedLanguage_example"; // String | 
List<String> includes = Arrays.asList("includes_example"); // List<String> | 
String orderName = "orderName_example"; // String | 
String orderCreatedAt = "orderCreatedAt_example"; // String | 
String orderUpdatedAt = "orderUpdatedAt_example"; // String | 
String orderFollowedCount = "orderFollowedCount_example"; // String | 
String orderRelevance = "orderRelevance_example"; // String | 
try {
    ScanlationGroupList result = apiInstance.getSearchGroup(limit, offset, ids, name, focusedLanguage, includes, orderName, orderCreatedAt, orderUpdatedAt, orderFollowedCount, orderRelevance);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ScanlationGroupApi#getSearchGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Integer**|  | [optional] [default to 10] [enum: ]
 **offset** | **Integer**|  | [optional] [enum: ]
 **ids** | [**List&lt;UUID&gt;**](UUID.md)| ScanlationGroup ids (limited to 100 per request) | [optional]
 **name** | **String**|  | [optional]
 **focusedLanguage** | **String**|  | [optional]
 **includes** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **orderName** | **String**|  | [optional] [enum: asc, desc]
 **orderCreatedAt** | **String**|  | [optional] [enum: asc, desc]
 **orderUpdatedAt** | **String**|  | [optional] [enum: asc, desc]
 **orderFollowedCount** | **String**|  | [optional] [enum: asc, desc]
 **orderRelevance** | **String**|  | [optional] [enum: asc, desc]

### Return type

[**ScanlationGroupList**](ScanlationGroupList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postGroup"></a>
# **postGroup**
> ScanlationGroupResponse postGroup(contentType, body)

Create Scanlation Group

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ScanlationGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


ScanlationGroupApi apiInstance = new ScanlationGroupApi();
String contentType = "application/json"; // String | 
CreateScanlationGroup body = new CreateScanlationGroup(); // CreateScanlationGroup | The size of the body is limited to 16KB.
try {
    ScanlationGroupResponse result = apiInstance.postGroup(contentType, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ScanlationGroupApi#postGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **body** | [**CreateScanlationGroup**](CreateScanlationGroup.md)| The size of the body is limited to 16KB. | [optional]

### Return type

[**ScanlationGroupResponse**](ScanlationGroupResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postGroupIdFollow"></a>
# **postGroupIdFollow**
> Response postGroupIdFollow(id)

Follow Scanlation Group

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ScanlationGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


ScanlationGroupApi apiInstance = new ScanlationGroupApi();
UUID id = new UUID(); // UUID | 
try {
    Response result = apiInstance.postGroupIdFollow(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ScanlationGroupApi#postGroupIdFollow");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)|  |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="putGroupId"></a>
# **putGroupId**
> ScanlationGroupResponse putGroupId(contentType, id, body)

Update Scanlation Group

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ScanlationGroupApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


ScanlationGroupApi apiInstance = new ScanlationGroupApi();
String contentType = "application/json"; // String | 
UUID id = new UUID(); // UUID | Scanlation Group ID
ScanlationGroupEdit body = new ScanlationGroupEdit(); // ScanlationGroupEdit | The size of the body is limited to 8KB.
try {
    ScanlationGroupResponse result = apiInstance.putGroupId(contentType, id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ScanlationGroupApi#putGroupId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **id** | [**UUID**](.md)| Scanlation Group ID |
 **body** | [**ScanlationGroupEdit**](ScanlationGroupEdit.md)| The size of the body is limited to 8KB. | [optional]

### Return type

[**ScanlationGroupResponse**](ScanlationGroupResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

