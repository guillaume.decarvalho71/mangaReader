# ChapterReadMarkerApi

All URIs are relative to *https://api.mangadex.org*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getMangaChapterReadmarkers**](ChapterReadMarkerApi.md#getMangaChapterReadmarkers) | **GET** manga/{id}/read | Manga read markers
[**getMangaChapterReadmarkers2**](ChapterReadMarkerApi.md#getMangaChapterReadmarkers2) | **GET** manga/read | Manga read markers
[**getReadingHistory**](ChapterReadMarkerApi.md#getReadingHistory) | **GET** user/history | Get users reading history
[**postMangaChapterReadmarkers**](ChapterReadMarkerApi.md#postMangaChapterReadmarkers) | **POST** manga/{id}/read | Manga read markers batch

<a name="getMangaChapterReadmarkers"></a>
# **getMangaChapterReadmarkers**
> InlineResponse2003 getMangaChapterReadmarkers(id)

Manga read markers

A list of chapter ids that are marked as read for the specified manga

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ChapterReadMarkerApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


ChapterReadMarkerApi apiInstance = new ChapterReadMarkerApi();
UUID id = new UUID(); // UUID | 
try {
    InlineResponse2003 result = apiInstance.getMangaChapterReadmarkers(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChapterReadMarkerApi#getMangaChapterReadmarkers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)|  |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getMangaChapterReadmarkers2"></a>
# **getMangaChapterReadmarkers2**
> InlineResponse2004 getMangaChapterReadmarkers2(ids, grouped)

Manga read markers

A list of chapter ids that are marked as read for the given manga ids

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ChapterReadMarkerApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


ChapterReadMarkerApi apiInstance = new ChapterReadMarkerApi();
List<UUID> ids = Arrays.asList(new UUID()); // List<UUID> | Manga ids
Boolean grouped = true; // Boolean | Group results by manga ids
try {
    InlineResponse2004 result = apiInstance.getMangaChapterReadmarkers2(ids, grouped);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChapterReadMarkerApi#getMangaChapterReadmarkers2");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ids** | [**List&lt;UUID&gt;**](UUID.md)| Manga ids |
 **grouped** | **Boolean**| Group results by manga ids | [optional]

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReadingHistory"></a>
# **getReadingHistory**
> InlineResponse20016 getReadingHistory()

Get users reading history

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ChapterReadMarkerApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


ChapterReadMarkerApi apiInstance = new ChapterReadMarkerApi();
try {
    InlineResponse20016 result = apiInstance.getReadingHistory();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChapterReadMarkerApi#getReadingHistory");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20016**](InlineResponse20016.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postMangaChapterReadmarkers"></a>
# **postMangaChapterReadmarkers**
> InlineResponse2002 postMangaChapterReadmarkers(id, body, updateHistory)

Manga read markers batch

Send a lot of chapter ids for one manga to mark as read and/or unread

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ChapterReadMarkerApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


ChapterReadMarkerApi apiInstance = new ChapterReadMarkerApi();
UUID id = new UUID(); // UUID | 
ChapterReadMarkerBatch body = new ChapterReadMarkerBatch(); // ChapterReadMarkerBatch | The size of the body is limited to 10KB.
Boolean updateHistory = true; // Boolean | Adding this will cause the chapter to be stored in the user's reading history
try {
    InlineResponse2002 result = apiInstance.postMangaChapterReadmarkers(id, body, updateHistory);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChapterReadMarkerApi#postMangaChapterReadmarkers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)|  |
 **body** | [**ChapterReadMarkerBatch**](ChapterReadMarkerBatch.md)| The size of the body is limited to 10KB. | [optional]
 **updateHistory** | **Boolean**| Adding this will cause the chapter to be stored in the user&#x27;s reading history | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

