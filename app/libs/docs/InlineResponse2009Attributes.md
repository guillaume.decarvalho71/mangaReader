# InlineResponse2009Attributes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reason** | [**LocalizedString**](LocalizedString.md) |  |  [optional]
**detailsRequired** | **Boolean** |  |  [optional]
**category** | [**CategoryEnum**](#CategoryEnum) |  |  [optional]
**version** | **Integer** |  |  [optional]

<a name="CategoryEnum"></a>
## Enum: CategoryEnum
Name | Value
---- | -----
MANGA | &quot;manga&quot;
CHAPTER | &quot;chapter&quot;
SCANLATION_GROUP | &quot;scanlation_group&quot;
USER | &quot;user&quot;
AUTHOR | &quot;author&quot;
