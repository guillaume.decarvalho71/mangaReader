# InfrastructureApi

All URIs are relative to *https://api.mangadex.org*

Method | HTTP request | Description
------------- | ------------- | -------------
[**pingGet**](InfrastructureApi.md#pingGet) | **GET** ping | Ping the server

<a name="pingGet"></a>
# **pingGet**
> String pingGet()

Ping the server

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.InfrastructureApi;


InfrastructureApi apiInstance = new InfrastructureApi();
try {
    String result = apiInstance.pingGet();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling InfrastructureApi#pingGet");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

