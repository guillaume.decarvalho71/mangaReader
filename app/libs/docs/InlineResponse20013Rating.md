# InlineResponse20013Rating

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**average** | [**BigDecimal**](BigDecimal.md) | Will be nullable if no ratings has been done |  [optional]
**bayesian** | [**BigDecimal**](BigDecimal.md) | Average weighted on all the Manga population |  [optional]
