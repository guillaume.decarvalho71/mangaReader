# CoverEdit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**volume** | **String** |  | 
**description** | **String** |  |  [optional]
**locale** | **String** |  |  [optional]
**version** | **Integer** |  | 
