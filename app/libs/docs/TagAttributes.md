# TagAttributes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | [**LocalizedString**](LocalizedString.md) |  |  [optional]
**description** | [**LocalizedString**](LocalizedString.md) |  |  [optional]
**group** | [**GroupEnum**](#GroupEnum) |  |  [optional]
**version** | **Integer** |  |  [optional]

<a name="GroupEnum"></a>
## Enum: GroupEnum
Name | Value
---- | -----
CONTENT | &quot;content&quot;
FORMAT | &quot;format&quot;
GENRE | &quot;genre&quot;
THEME | &quot;theme&quot;
