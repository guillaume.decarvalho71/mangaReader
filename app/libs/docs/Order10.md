# Order10

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | [**CreatedAtEnum**](#CreatedAtEnum) |  |  [optional]

<a name="CreatedAtEnum"></a>
## Enum: CreatedAtEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;
