# InlineResponse200Chapters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chapter** | **String** |  |  [optional]
**id** | [**UUID**](UUID.md) |  |  [optional]
**others** | [**List&lt;UUID&gt;**](UUID.md) |  |  [optional]
**count** | **Integer** |  |  [optional]
