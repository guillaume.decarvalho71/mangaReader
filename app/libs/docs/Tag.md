# Tag

## Properties
| Name              | Type                                            | Description | Notes      |
|-------------------|-------------------------------------------------|-------------|------------|
| **id**            | [**UUID**](UUID.md)                             |             | [optional] |
| **type**          | [**TypeEnum**](#TypeEnum)                       |             | [optional] |
| **attributes**    | [**TagAttributes**](TagAttributes.md)           |             | [optional] |
| **relationships** | [**List&lt;Relationship&gt;**](Relationship.md) |             | [optional] |

<a name="TypeEnum"></a>
## Enum: TypeEnum
| Name | Value           |
|------|-----------------|
| TAG  | &quot;tag&quot; |
