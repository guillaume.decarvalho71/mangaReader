# FeedApi

All URIs are relative to *https://api.mangadex.org*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getListIdFeed**](FeedApi.md#getListIdFeed) | **GET** list/{id}/feed | CustomList Manga feed
[**getUserFollowsMangaFeed**](FeedApi.md#getUserFollowsMangaFeed) | **GET** user/follows/manga/feed | Get logged User followed Manga feed (Chapter list)

<a name="getListIdFeed"></a>
# **getListIdFeed**
> ChapterList getListIdFeed(id, limit, offset, translatedLanguage, originalLanguage, excludedOriginalLanguage, contentRating, excludedGroups, excludedUploaders, includeFutureUpdates, createdAtSince, updatedAtSince, publishAtSince, orderCreatedAt, orderUpdatedAt, orderPublishAt, orderReadableAt, orderVolume, orderChapter, includes, includeEmptyPages, includeFuturePublishAt, includeExternalUrl)

CustomList Manga feed

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.FeedApi;


FeedApi apiInstance = new FeedApi();
UUID id = new UUID(); // UUID | 
Integer limit = 100; // Integer | 
Integer offset = 56; // Integer | 
List<String> translatedLanguage = Arrays.asList("translatedLanguage_example"); // List<String> | 
List<String> originalLanguage = Arrays.asList("originalLanguage_example"); // List<String> | 
List<String> excludedOriginalLanguage = Arrays.asList("excludedOriginalLanguage_example"); // List<String> | 
List<String> contentRating = Arrays.asList("[\"safe\",\"suggestive\",\"erotica\"]"); // List<String> | 
List<UUID> excludedGroups = Arrays.asList(new UUID()); // List<UUID> | 
List<UUID> excludedUploaders = Arrays.asList(new UUID()); // List<UUID> | 
String includeFutureUpdates = "1"; // String | 
String createdAtSince = "createdAtSince_example"; // String | 
String updatedAtSince = "updatedAtSince_example"; // String | 
String publishAtSince = "publishAtSince_example"; // String | 
String orderCreatedAt = "orderCreatedAt_example"; // String | 
String orderUpdatedAt = "orderUpdatedAt_example"; // String | 
String orderPublishAt = "orderPublishAt_example"; // String | 
String orderReadableAt = "orderReadableAt_example"; // String | 
String orderVolume = "orderVolume_example"; // String | 
String orderChapter = "orderChapter_example"; // String | 
List<String> includes = Arrays.asList("includes_example"); // List<String> | 
Integer includeEmptyPages = 56; // Integer | 
Integer includeFuturePublishAt = 56; // Integer | 
Integer includeExternalUrl = 56; // Integer | 
try {
    ChapterList result = apiInstance.getListIdFeed(id, limit, offset, translatedLanguage, originalLanguage, excludedOriginalLanguage, contentRating, excludedGroups, excludedUploaders, includeFutureUpdates, createdAtSince, updatedAtSince, publishAtSince, orderCreatedAt, orderUpdatedAt, orderPublishAt, orderReadableAt, orderVolume, orderChapter, includes, includeEmptyPages, includeFuturePublishAt, includeExternalUrl);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FeedApi#getListIdFeed");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)|  |
 **limit** | **Integer**|  | [optional] [default to 100] [enum: ]
 **offset** | **Integer**|  | [optional] [enum: ]
 **translatedLanguage** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **originalLanguage** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **excludedOriginalLanguage** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **contentRating** | [**List&lt;String&gt;**](String.md)|  | [optional] [default to [&quot;safe&quot;,&quot;suggestive&quot;,&quot;erotica&quot;]] [enum: safe, suggestive, erotica, pornographic]
 **excludedGroups** | [**List&lt;UUID&gt;**](UUID.md)|  | [optional]
 **excludedUploaders** | [**List&lt;UUID&gt;**](UUID.md)|  | [optional]
 **includeFutureUpdates** | **String**|  | [optional] [default to 1] [enum: 0, 1]
 **createdAtSince** | **String**|  | [optional]
 **updatedAtSince** | **String**|  | [optional]
 **publishAtSince** | **String**|  | [optional]
 **orderCreatedAt** | **String**|  | [optional] [enum: asc, desc]
 **orderUpdatedAt** | **String**|  | [optional] [enum: asc, desc]
 **orderPublishAt** | **String**|  | [optional] [enum: asc, desc]
 **orderReadableAt** | **String**|  | [optional] [enum: asc, desc]
 **orderVolume** | **String**|  | [optional] [enum: asc, desc]
 **orderChapter** | **String**|  | [optional] [enum: asc, desc]
 **includes** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **includeEmptyPages** | **Integer**|  | [optional] [enum: 0, 1]
 **includeFuturePublishAt** | **Integer**|  | [optional] [enum: 0, 1]
 **includeExternalUrl** | **Integer**|  | [optional] [enum: 0, 1]

### Return type

[**ChapterList**](ChapterList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserFollowsMangaFeed"></a>
# **getUserFollowsMangaFeed**
> ChapterList getUserFollowsMangaFeed(limit, offset, translatedLanguage, originalLanguage, excludedOriginalLanguage, contentRating, excludedGroups, excludedUploaders, includeFutureUpdates, createdAtSince, updatedAtSince, publishAtSince, orderCreatedAt, orderUpdatedAt, orderPublishAt, orderReadableAt, orderVolume, orderChapter, includes, includeEmptyPages, includeFuturePublishAt, includeExternalUrl)

Get logged User followed Manga feed (Chapter list)

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FeedApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


FeedApi apiInstance = new FeedApi();
Integer limit = 100; // Integer | 
Integer offset = 56; // Integer | 
List<String> translatedLanguage = Arrays.asList("translatedLanguage_example"); // List<String> | 
List<String> originalLanguage = Arrays.asList("originalLanguage_example"); // List<String> | 
List<String> excludedOriginalLanguage = Arrays.asList("excludedOriginalLanguage_example"); // List<String> | 
List<String> contentRating = Arrays.asList("[\"safe\",\"suggestive\",\"erotica\"]"); // List<String> | 
List<UUID> excludedGroups = Arrays.asList(new UUID()); // List<UUID> | 
List<UUID> excludedUploaders = Arrays.asList(new UUID()); // List<UUID> | 
String includeFutureUpdates = "1"; // String | 
String createdAtSince = "createdAtSince_example"; // String | 
String updatedAtSince = "updatedAtSince_example"; // String | 
String publishAtSince = "publishAtSince_example"; // String | 
String orderCreatedAt = "orderCreatedAt_example"; // String | 
String orderUpdatedAt = "orderUpdatedAt_example"; // String | 
String orderPublishAt = "orderPublishAt_example"; // String | 
String orderReadableAt = "orderReadableAt_example"; // String | 
String orderVolume = "orderVolume_example"; // String | 
String orderChapter = "orderChapter_example"; // String | 
List<String> includes = Arrays.asList("includes_example"); // List<String> | 
Integer includeEmptyPages = 56; // Integer | 
Integer includeFuturePublishAt = 56; // Integer | 
Integer includeExternalUrl = 56; // Integer | 
try {
    ChapterList result = apiInstance.getUserFollowsMangaFeed(limit, offset, translatedLanguage, originalLanguage, excludedOriginalLanguage, contentRating, excludedGroups, excludedUploaders, includeFutureUpdates, createdAtSince, updatedAtSince, publishAtSince, orderCreatedAt, orderUpdatedAt, orderPublishAt, orderReadableAt, orderVolume, orderChapter, includes, includeEmptyPages, includeFuturePublishAt, includeExternalUrl);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FeedApi#getUserFollowsMangaFeed");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Integer**|  | [optional] [default to 100] [enum: ]
 **offset** | **Integer**|  | [optional] [enum: ]
 **translatedLanguage** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **originalLanguage** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **excludedOriginalLanguage** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **contentRating** | [**List&lt;String&gt;**](String.md)|  | [optional] [default to [&quot;safe&quot;,&quot;suggestive&quot;,&quot;erotica&quot;]] [enum: safe, suggestive, erotica, pornographic]
 **excludedGroups** | [**List&lt;UUID&gt;**](UUID.md)|  | [optional]
 **excludedUploaders** | [**List&lt;UUID&gt;**](UUID.md)|  | [optional]
 **includeFutureUpdates** | **String**|  | [optional] [default to 1] [enum: 0, 1]
 **createdAtSince** | **String**|  | [optional]
 **updatedAtSince** | **String**|  | [optional]
 **publishAtSince** | **String**|  | [optional]
 **orderCreatedAt** | **String**|  | [optional] [enum: asc, desc]
 **orderUpdatedAt** | **String**|  | [optional] [enum: asc, desc]
 **orderPublishAt** | **String**|  | [optional] [enum: asc, desc]
 **orderReadableAt** | **String**|  | [optional] [enum: asc, desc]
 **orderVolume** | **String**|  | [optional] [enum: asc, desc]
 **orderChapter** | **String**|  | [optional] [enum: asc, desc]
 **includes** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **includeEmptyPages** | **Integer**|  | [optional] [enum: 0, 1]
 **includeFuturePublishAt** | **Integer**|  | [optional] [enum: 0, 1]
 **includeExternalUrl** | **Integer**|  | [optional] [enum: 0, 1]

### Return type

[**ChapterList**](ChapterList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

