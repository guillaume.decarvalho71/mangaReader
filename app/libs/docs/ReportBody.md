# ReportBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | [**CategoryEnum**](#CategoryEnum) |  |  [optional]
**reason** | [**UUID**](UUID.md) |  |  [optional]
**objectId** | [**UUID**](UUID.md) |  |  [optional]
**details** | **String** |  |  [optional]

<a name="CategoryEnum"></a>
## Enum: CategoryEnum
Name | Value
---- | -----
MANGA | &quot;manga&quot;
CHAPTER | &quot;chapter&quot;
USER | &quot;user&quot;
SCANLATION_GROUP | &quot;scanlation_group&quot;
AUTHOR | &quot;author&quot;
