# ChapterApi

All URIs are relative to *https://api.mangadex.org*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteChapterId**](ChapterApi.md#deleteChapterId) | **DELETE** chapter/{id} | Delete Chapter
[**getChapter**](ChapterApi.md#getChapter) | **GET** chapter | Chapter list
[**getChapterId**](ChapterApi.md#getChapterId) | **GET** chapter/{id} | Get Chapter
[**putChapterId**](ChapterApi.md#putChapterId) | **PUT** chapter/{id} | Update Chapter

<a name="deleteChapterId"></a>
# **deleteChapterId**
> Response deleteChapterId(id)

Delete Chapter

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ChapterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


ChapterApi apiInstance = new ChapterApi();
UUID id = new UUID(); // UUID | Chapter ID
try {
    Response result = apiInstance.deleteChapterId(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChapterApi#deleteChapterId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| Chapter ID |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getChapter"></a>
# **getChapter**
> ChapterList getChapter(limit, offset, ids, title, groups, uploader, manga, volume, chapter, translatedLanguage, originalLanguage, excludedOriginalLanguage, contentRating, excludedGroups, excludedUploaders, includeFutureUpdates, includeEmptyPages, includeFuturePublishAt, includeExternalUrl, createdAtSince, updatedAtSince, publishAtSince, orderCreatedAt, orderUpdatedAt, orderPublishAt, orderReadableAt, orderVolume, orderChapter, includes)

Chapter list

Chapter list. If you want the Chapters of a given Manga, please check the feed endpoints.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ChapterApi;


ChapterApi apiInstance = new ChapterApi();
Integer limit = 10; // Integer | 
Integer offset = 56; // Integer | 
List<UUID> ids = Arrays.asList(new UUID()); // List<UUID> | Chapter ids (limited to 100 per request)
String title = "title_example"; // String | 
List<UUID> groups = Arrays.asList(new UUID()); // List<UUID> | 
Uploader uploader = new Uploader(); // Uploader | 
UUID manga = new UUID(); // UUID | 
Volume volume = new Volume(); // Volume | 
Chapter chapter = new Chapter(); // Chapter | 
List<String> translatedLanguage = Arrays.asList("translatedLanguage_example"); // List<String> | 
List<String> originalLanguage = Arrays.asList("originalLanguage_example"); // List<String> | 
List<String> excludedOriginalLanguage = Arrays.asList("excludedOriginalLanguage_example"); // List<String> | 
List<String> contentRating = Arrays.asList("[\"safe\",\"suggestive\",\"erotica\"]"); // List<String> | 
List<UUID> excludedGroups = Arrays.asList(new UUID()); // List<UUID> | 
List<UUID> excludedUploaders = Arrays.asList(new UUID()); // List<UUID> | 
String includeFutureUpdates = "1"; // String | 
Integer includeEmptyPages = 56; // Integer | 
Integer includeFuturePublishAt = 56; // Integer | 
Integer includeExternalUrl = 56; // Integer | 
String createdAtSince = "createdAtSince_example"; // String | 
String updatedAtSince = "updatedAtSince_example"; // String | 
String publishAtSince = "publishAtSince_example"; // String | 
String orderCreatedAt = "orderCreatedAt_example"; // String | 
String orderUpdatedAt = "orderUpdatedAt_example"; // String | 
String orderPublishAt = "orderPublishAt_example"; // String | 
String orderReadableAt = "orderReadableAt_example"; // String | 
String orderVolume = "orderVolume_example"; // String | 
String orderChapter = "orderChapter_example"; // String | 
List<String> includes = Arrays.asList("includes_example"); // List<String> | 
try {
    ChapterList result = apiInstance.getChapter(limit, offset, ids, title, groups, uploader, manga, volume, chapter, translatedLanguage, originalLanguage, excludedOriginalLanguage, contentRating, excludedGroups, excludedUploaders, includeFutureUpdates, includeEmptyPages, includeFuturePublishAt, includeExternalUrl, createdAtSince, updatedAtSince, publishAtSince, orderCreatedAt, orderUpdatedAt, orderPublishAt, orderReadableAt, orderVolume, orderChapter, includes);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChapterApi#getChapter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Integer**|  | [optional] [default to 10] [enum: ]
 **offset** | **Integer**|  | [optional] [enum: ]
 **ids** | [**List&lt;UUID&gt;**](UUID.md)| Chapter ids (limited to 100 per request) | [optional]
 **title** | **String**|  | [optional]
 **groups** | [**List&lt;UUID&gt;**](UUID.md)|  | [optional]
 **uploader** | [**Uploader**](.md)|  | [optional]
 **manga** | [**UUID**](.md)|  | [optional]
 **volume** | [**Volume**](.md)|  | [optional]
 **chapter** | [**Chapter**](.md)|  | [optional]
 **translatedLanguage** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **originalLanguage** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **excludedOriginalLanguage** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **contentRating** | [**List&lt;String&gt;**](String.md)|  | [optional] [default to [&quot;safe&quot;,&quot;suggestive&quot;,&quot;erotica&quot;]] [enum: safe, suggestive, erotica, pornographic]
 **excludedGroups** | [**List&lt;UUID&gt;**](UUID.md)|  | [optional]
 **excludedUploaders** | [**List&lt;UUID&gt;**](UUID.md)|  | [optional]
 **includeFutureUpdates** | **String**|  | [optional] [default to 1] [enum: 0, 1]
 **includeEmptyPages** | **Integer**|  | [optional] [enum: 0, 1]
 **includeFuturePublishAt** | **Integer**|  | [optional] [enum: 0, 1]
 **includeExternalUrl** | **Integer**|  | [optional] [enum: 0, 1]
 **createdAtSince** | **String**|  | [optional]
 **updatedAtSince** | **String**|  | [optional]
 **publishAtSince** | **String**|  | [optional]
 **orderCreatedAt** | **String**|  | [optional] [enum: asc, desc]
 **orderUpdatedAt** | **String**|  | [optional] [enum: asc, desc]
 **orderPublishAt** | **String**|  | [optional] [enum: asc, desc]
 **orderReadableAt** | **String**|  | [optional] [enum: asc, desc]
 **orderVolume** | **String**|  | [optional] [enum: asc, desc]
 **orderChapter** | **String**|  | [optional] [enum: asc, desc]
 **includes** | [**List&lt;String&gt;**](String.md)|  | [optional]

### Return type

[**ChapterList**](ChapterList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getChapterId"></a>
# **getChapterId**
> ChapterResponse getChapterId(id, includes)

Get Chapter

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.ChapterApi;


ChapterApi apiInstance = new ChapterApi();
UUID id = new UUID(); // UUID | Chapter ID
List<String> includes = Arrays.asList("includes_example"); // List<String> | 
try {
    ChapterResponse result = apiInstance.getChapterId(id, includes);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChapterApi#getChapterId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| Chapter ID |
 **includes** | [**List&lt;String&gt;**](String.md)|  | [optional]

### Return type

[**ChapterResponse**](ChapterResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="putChapterId"></a>
# **putChapterId**
> ChapterResponse putChapterId(contentType, id, body)

Update Chapter

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ChapterApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


ChapterApi apiInstance = new ChapterApi();
String contentType = "application/json"; // String | 
UUID id = new UUID(); // UUID | Chapter ID
ChapterEdit body = new ChapterEdit(); // ChapterEdit | The size of the body is limited to 32KB.
try {
    ChapterResponse result = apiInstance.putChapterId(contentType, id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChapterApi#putChapterId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **id** | [**UUID**](.md)| Chapter ID |
 **body** | [**ChapterEdit**](ChapterEdit.md)| The size of the body is limited to 32KB. | [optional]

### Return type

[**ChapterResponse**](ChapterResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

