# CreateScanlationGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**website** | **String** |  |  [optional]
**ircServer** | **String** |  |  [optional]
**ircChannel** | **String** |  |  [optional]
**discord** | **String** |  |  [optional]
**contactEmail** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**twitter** | **String** |  |  [optional]
**mangaUpdates** | **String** |  |  [optional]
**inactive** | **Boolean** |  |  [optional]
**publishDelay** | **String** |  |  [optional]
