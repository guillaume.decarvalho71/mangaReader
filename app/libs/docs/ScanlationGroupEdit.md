# ScanlationGroupEdit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**leader** | [**UUID**](UUID.md) |  |  [optional]
**members** | [**List&lt;UUID&gt;**](UUID.md) |  |  [optional]
**website** | **String** |  |  [optional]
**ircServer** | **String** |  |  [optional]
**ircChannel** | **String** |  |  [optional]
**discord** | **String** |  |  [optional]
**contactEmail** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**twitter** | **String** |  |  [optional]
**mangaUpdates** | **String** |  |  [optional]
**focusedLanguages** | **List&lt;String&gt;** |  |  [optional]
**inactive** | **Boolean** |  |  [optional]
**locked** | **Boolean** |  |  [optional]
**publishDelay** | **String** |  |  [optional]
**version** | **Integer** |  | 
