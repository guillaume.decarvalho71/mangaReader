# InlineResponse20014

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | **String** |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**settings** | **Object** | Settings that were validated by linked template |  [optional]
**template** | [**UUID**](UUID.md) | Settings template UUID |  [optional]
