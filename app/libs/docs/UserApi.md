# UserApi

All URIs are relative to *https://api.mangadex.org*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteUserId**](UserApi.md#deleteUserId) | **DELETE** user/{id} | Delete User
[**getUser**](UserApi.md#getUser) | **GET** user | User list
[**getUserId**](UserApi.md#getUserId) | **GET** user/{id} | Get User
[**getUserMe**](UserApi.md#getUserMe) | **GET** user/me | Logged User details
[**postUserDeleteCode**](UserApi.md#postUserDeleteCode) | **POST** user/delete/{code} | Approve User deletion
[**postUserEmail**](UserApi.md#postUserEmail) | **POST** user/email | Update User email
[**postUserPassword**](UserApi.md#postUserPassword) | **POST** user/password | Update User password

<a name="deleteUserId"></a>
# **deleteUserId**
> Response deleteUserId(id)

Delete User

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.UserApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


UserApi apiInstance = new UserApi();
UUID id = new UUID(); // UUID | User ID
try {
    Response result = apiInstance.deleteUserId(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#deleteUserId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| User ID |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUser"></a>
# **getUser**
> UserList getUser(limit, offset, ids, username, orderUsername)

User list

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.UserApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


UserApi apiInstance = new UserApi();
Integer limit = 10; // Integer | 
Integer offset = 56; // Integer | 
List<UUID> ids = Arrays.asList(new UUID()); // List<UUID> | User ids (limited to 100 per request)
String username = "username_example"; // String | 
String orderUsername = "orderUsername_example"; // String | 
try {
    UserList result = apiInstance.getUser(limit, offset, ids, username, orderUsername);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#getUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Integer**|  | [optional] [default to 10] [enum: ]
 **offset** | **Integer**|  | [optional] [enum: ]
 **ids** | [**List&lt;UUID&gt;**](UUID.md)| User ids (limited to 100 per request) | [optional]
 **username** | **String**|  | [optional]
 **orderUsername** | **String**|  | [optional] [enum: asc, desc]

### Return type

[**UserList**](UserList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserId"></a>
# **getUserId**
> UserResponse getUserId(id)

Get User

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.UserApi;


UserApi apiInstance = new UserApi();
UUID id = new UUID(); // UUID | User ID
try {
    UserResponse result = apiInstance.getUserId(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#getUserId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| User ID |

### Return type

[**UserResponse**](UserResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserMe"></a>
# **getUserMe**
> UserResponse getUserMe()

Logged User details

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.UserApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


UserApi apiInstance = new UserApi();
try {
    UserResponse result = apiInstance.getUserMe();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#getUserMe");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UserResponse**](UserResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postUserDeleteCode"></a>
# **postUserDeleteCode**
> Response postUserDeleteCode(code)

Approve User deletion

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.UserApi;


UserApi apiInstance = new UserApi();
UUID code = new UUID(); // UUID | User delete code
try {
    Response result = apiInstance.postUserDeleteCode(code);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#postUserDeleteCode");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | [**UUID**](.md)| User delete code |

### Return type

[**Response**](Response.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postUserEmail"></a>
# **postUserEmail**
> Response postUserEmail(contentType, body)

Update User email

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.UserApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


UserApi apiInstance = new UserApi();
String contentType = "application/json"; // String | 
UserEmailBody body = new UserEmailBody(); // UserEmailBody | 
try {
    Response result = apiInstance.postUserEmail(contentType, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#postUserEmail");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **body** | [**UserEmailBody**](UserEmailBody.md)|  | [optional]

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postUserPassword"></a>
# **postUserPassword**
> Response postUserPassword(contentType, body)

Update User password

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.UserApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


UserApi apiInstance = new UserApi();
String contentType = "application/json"; // String | 
UserPasswordBody body = new UserPasswordBody(); // UserPasswordBody | 
try {
    Response result = apiInstance.postUserPassword(contentType, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#postUserPassword");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **body** | [**UserPasswordBody**](UserPasswordBody.md)|  | [optional]

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

