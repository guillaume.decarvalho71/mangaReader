# UploadSessionFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**UUID**](UUID.md) |  |  [optional]
**type** | [**TypeEnum**](#TypeEnum) |  |  [optional]
**attributes** | [**UploadSessionFileAttributes**](UploadSessionFileAttributes.md) |  |  [optional]

<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
UPLOAD_SESSION_FILE | &quot;upload_session_file&quot;
