# LegacyApi

All URIs are relative to *https://api.mangadex.org*

Method | HTTP request | Description
------------- | ------------- | -------------
[**postLegacyMapping**](LegacyApi.md#postLegacyMapping) | **POST** legacy/mapping | Legacy ID mapping

<a name="postLegacyMapping"></a>
# **postLegacyMapping**
> MappingIdResponse postLegacyMapping(contentType, body)

Legacy ID mapping

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.LegacyApi;


LegacyApi apiInstance = new LegacyApi();
String contentType = "application/json"; // String | 
MappingIdBody body = new MappingIdBody(); // MappingIdBody | The size of the body is limited to 10KB.
try {
    MappingIdResponse result = apiInstance.postLegacyMapping(contentType, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling LegacyApi#postLegacyMapping");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **body** | [**MappingIdBody**](MappingIdBody.md)| The size of the body is limited to 10KB. | [optional]

### Return type

[**MappingIdResponse**](MappingIdResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

