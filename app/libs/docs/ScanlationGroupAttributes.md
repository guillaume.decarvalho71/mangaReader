# ScanlationGroupAttributes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**altNames** | [**List&lt;LocalizedString&gt;**](LocalizedString.md) |  |  [optional]
**website** | **String** |  |  [optional]
**ircServer** | **String** |  |  [optional]
**ircChannel** | **String** |  |  [optional]
**discord** | **String** |  |  [optional]
**contactEmail** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**twitter** | **String** |  |  [optional]
**mangaUpdates** | **String** |  |  [optional]
**focusedLanguage** | **List&lt;String&gt;** |  |  [optional]
**locked** | **Boolean** |  |  [optional]
**official** | **Boolean** |  |  [optional]
**inactive** | **Boolean** |  |  [optional]
**publishDelay** | **String** | Should respected ISO 8601 duration specification: https://en.wikipedia.org/wiki/ISO_8601#Durations |  [optional]
**version** | **Integer** |  |  [optional]
**createdAt** | **String** |  |  [optional]
**updatedAt** | **String** |  |  [optional]
