# InlineResponse20010

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**ResultEnum**](#ResultEnum) |  |  [optional]
**errors** | [**List&lt;Error&gt;**](Error.md) |  |  [optional]
**data** | [**List&lt;UploadSessionFile&gt;**](UploadSessionFile.md) |  |  [optional]

<a name="ResultEnum"></a>
## Enum: ResultEnum
Name | Value
---- | -----
OK | &quot;ok&quot;
ERROR | &quot;error&quot;
