# CommitUploadSession

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chapterDraft** | [**ChapterDraft**](ChapterDraft.md) |  |  [optional]
**pageOrder** | [**List&lt;UUID&gt;**](UUID.md) | ordered list of Upload Session File ids |  [optional]
