# InlineResponse2003

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**ResultEnum**](#ResultEnum) |  |  [optional]
**data** | [**List&lt;UUID&gt;**](UUID.md) |  |  [optional]

<a name="ResultEnum"></a>
## Enum: ResultEnum
Name | Value
---- | -----
OK | &quot;ok&quot;
