# LoginResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**ResultEnum**](#ResultEnum) |  |  [optional]
**token** | [**LoginResponseToken**](LoginResponseToken.md) |  |  [optional]

<a name="ResultEnum"></a>
## Enum: ResultEnum
Name | Value
---- | -----
OK | &quot;ok&quot;
ERROR | &quot;error&quot;
