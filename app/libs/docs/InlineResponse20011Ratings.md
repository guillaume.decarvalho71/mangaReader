# InlineResponse20011Ratings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rating** | **Integer** |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
