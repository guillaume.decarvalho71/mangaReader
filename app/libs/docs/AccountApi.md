# AccountApi

All URIs are relative to *https://api.mangadex.org*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAccountActivateCode**](AccountApi.md#getAccountActivateCode) | **POST** account/activate/{code} | Activate account
[**getAccountAvailable**](AccountApi.md#getAccountAvailable) | **GET** account/available | Account username available
[**postAccountActivateResend**](AccountApi.md#postAccountActivateResend) | **POST** account/activate/resend | Resend Activation code
[**postAccountCreate**](AccountApi.md#postAccountCreate) | **POST** account/create | Create Account
[**postAccountRecover**](AccountApi.md#postAccountRecover) | **POST** account/recover | Recover given Account
[**postAccountRecoverCode**](AccountApi.md#postAccountRecoverCode) | **POST** account/recover/{code} | Complete Account recover

<a name="getAccountActivateCode"></a>
# **getAccountActivateCode**
> AccountActivateResponse getAccountActivateCode(code)

Activate account

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.AccountApi;


AccountApi apiInstance = new AccountApi();
String code = "code_example"; // String | 
try {
    AccountActivateResponse result = apiInstance.getAccountActivateCode(code);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountApi#getAccountActivateCode");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **String**|  |

### Return type

[**AccountActivateResponse**](AccountActivateResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAccountAvailable"></a>
# **getAccountAvailable**
> InlineResponse2001 getAccountAvailable(username)

Account username available

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.AccountApi;


AccountApi apiInstance = new AccountApi();
String username = "username_example"; // String | Username to check for avaibility
try {
    InlineResponse2001 result = apiInstance.getAccountAvailable(username);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountApi#getAccountAvailable");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| Username to check for avaibility |

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postAccountActivateResend"></a>
# **postAccountActivateResend**
> AccountActivateResponse postAccountActivateResend(contentType, body)

Resend Activation code

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.AccountApi;


AccountApi apiInstance = new AccountApi();
String contentType = "application/json"; // String | 
SendAccountActivationCode body = new SendAccountActivationCode(); // SendAccountActivationCode | The size of the body is limited to 1KB.
try {
    AccountActivateResponse result = apiInstance.postAccountActivateResend(contentType, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountApi#postAccountActivateResend");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **body** | [**SendAccountActivationCode**](SendAccountActivationCode.md)| The size of the body is limited to 1KB. | [optional]

### Return type

[**AccountActivateResponse**](AccountActivateResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postAccountCreate"></a>
# **postAccountCreate**
> UserResponse postAccountCreate(contentType, body)

Create Account

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.AccountApi;


AccountApi apiInstance = new AccountApi();
String contentType = "application/json"; // String | 
CreateAccount body = new CreateAccount(); // CreateAccount | The size of the body is limited to 4KB.
try {
    UserResponse result = apiInstance.postAccountCreate(contentType, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountApi#postAccountCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **body** | [**CreateAccount**](CreateAccount.md)| The size of the body is limited to 4KB. | [optional]

### Return type

[**UserResponse**](UserResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postAccountRecover"></a>
# **postAccountRecover**
> AccountActivateResponse postAccountRecover(contentType, body)

Recover given Account

You can only request Account Recovery once per Hour for the same Email Address

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.AccountApi;


AccountApi apiInstance = new AccountApi();
String contentType = "application/json"; // String | 
SendAccountActivationCode body = new SendAccountActivationCode(); // SendAccountActivationCode | The size of the body is limited to 1KB.
try {
    AccountActivateResponse result = apiInstance.postAccountRecover(contentType, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountApi#postAccountRecover");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **body** | [**SendAccountActivationCode**](SendAccountActivationCode.md)| The size of the body is limited to 1KB. | [optional]

### Return type

[**AccountActivateResponse**](AccountActivateResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postAccountRecoverCode"></a>
# **postAccountRecoverCode**
> AccountActivateResponse postAccountRecoverCode(contentType, code, body)

Complete Account recover

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.AccountApi;


AccountApi apiInstance = new AccountApi();
String contentType = "application/json"; // String | 
String code = "code_example"; // String | 
RecoverCompleteBody body = new RecoverCompleteBody(); // RecoverCompleteBody | The size of the body is limited to 2KB.
try {
    AccountActivateResponse result = apiInstance.postAccountRecoverCode(contentType, code, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountApi#postAccountRecoverCode");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **code** | **String**|  |
 **body** | [**RecoverCompleteBody**](RecoverCompleteBody.md)| The size of the body is limited to 2KB. | [optional]

### Return type

[**AccountActivateResponse**](AccountActivateResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

