# Order2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | [**UsernameEnum**](#UsernameEnum) |  |  [optional]

<a name="UsernameEnum"></a>
## Enum: UsernameEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;
