# RatingApi

All URIs are relative to *https://api.mangadex.org*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteRatingMangaId**](RatingApi.md#deleteRatingMangaId) | **DELETE** rating/{mangaId} | Delete Manga rating
[**getRating**](RatingApi.md#getRating) | **GET** rating | Get your ratings
[**postRatingMangaId**](RatingApi.md#postRatingMangaId) | **POST** rating/{mangaId} | Create or update Manga rating

<a name="deleteRatingMangaId"></a>
# **deleteRatingMangaId**
> Response deleteRatingMangaId(mangaId)

Delete Manga rating

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RatingApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


RatingApi apiInstance = new RatingApi();
UUID mangaId = new UUID(); // UUID | 
try {
    Response result = apiInstance.deleteRatingMangaId(mangaId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RatingApi#deleteRatingMangaId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mangaId** | [**UUID**](.md)|  |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getRating"></a>
# **getRating**
> InlineResponse20011 getRating()

Get your ratings

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RatingApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


RatingApi apiInstance = new RatingApi();
try {
    InlineResponse20011 result = apiInstance.getRating();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RatingApi#getRating");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20011**](InlineResponse20011.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postRatingMangaId"></a>
# **postRatingMangaId**
> Response postRatingMangaId(mangaId, body)

Create or update Manga rating

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.RatingApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


RatingApi apiInstance = new RatingApi();
UUID mangaId = new UUID(); // UUID | 
RatingMangaIdBody body = new RatingMangaIdBody(); // RatingMangaIdBody | 
try {
    Response result = apiInstance.postRatingMangaId(mangaId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RatingApi#postRatingMangaId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mangaId** | [**UUID**](.md)|  |
 **body** | [**RatingMangaIdBody**](RatingMangaIdBody.md)|  | [optional]

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

