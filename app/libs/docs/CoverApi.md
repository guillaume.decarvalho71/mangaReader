# CoverApi

All URIs are relative to *https://api.mangadex.org*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteCover**](CoverApi.md#deleteCover) | **DELETE** cover/{mangaOrCoverId} | Delete Cover
[**editCover**](CoverApi.md#editCover) | **PUT** cover/{mangaOrCoverId} | Edit Cover
[**getCover**](CoverApi.md#getCover) | **GET** cover | CoverArt list
[**getCoverId**](CoverApi.md#getCoverId) | **GET** cover/{mangaOrCoverId} | Get Cover
[**uploadCover**](CoverApi.md#uploadCover) | **POST** cover/{mangaOrCoverId} | Upload Cover

<a name="deleteCover"></a>
# **deleteCover**
> Response deleteCover(mangaOrCoverId)

Delete Cover

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CoverApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


CoverApi apiInstance = new CoverApi();
UUID mangaOrCoverId = new UUID(); // UUID | Is Manga UUID on POST
try {
    Response result = apiInstance.deleteCover(mangaOrCoverId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoverApi#deleteCover");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mangaOrCoverId** | [**UUID**](.md)| Is Manga UUID on POST |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="editCover"></a>
# **editCover**
> CoverResponse editCover(contentType, mangaOrCoverId, body)

Edit Cover

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CoverApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


CoverApi apiInstance = new CoverApi();
String contentType = "application/json"; // String | 
UUID mangaOrCoverId = new UUID(); // UUID | Is Manga UUID on POST
CoverEdit body = new CoverEdit(); // CoverEdit | The size of the body is limited to 2KB.
try {
    CoverResponse result = apiInstance.editCover(contentType, mangaOrCoverId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoverApi#editCover");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **mangaOrCoverId** | [**UUID**](.md)| Is Manga UUID on POST |
 **body** | [**CoverEdit**](CoverEdit.md)| The size of the body is limited to 2KB. | [optional]

### Return type

[**CoverResponse**](CoverResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getCover"></a>
# **getCover**
> CoverList getCover(limit, offset, manga, ids, uploaders, locales, orderCreatedAt, orderUpdatedAt, orderVolume, includes)

CoverArt list

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.CoverApi;


CoverApi apiInstance = new CoverApi();
Integer limit = 10; // Integer | 
Integer offset = 56; // Integer | 
List<UUID> manga = Arrays.asList(new UUID()); // List<UUID> | Manga ids (limited to 100 per request)
List<UUID> ids = Arrays.asList(new UUID()); // List<UUID> | Covers ids (limited to 100 per request)
List<UUID> uploaders = Arrays.asList(new UUID()); // List<UUID> | User ids (limited to 100 per request)
List<String> locales = Arrays.asList("locales_example"); // List<String> | Locales of cover art (limited to 100 per request)
String orderCreatedAt = "orderCreatedAt_example"; // String | 
String orderUpdatedAt = "orderUpdatedAt_example"; // String | 
String orderVolume = "orderVolume_example"; // String | 
List<String> includes = Arrays.asList("includes_example"); // List<String> | 
try {
    CoverList result = apiInstance.getCover(limit, offset, manga, ids, uploaders, locales, orderCreatedAt, orderUpdatedAt, orderVolume, includes);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoverApi#getCover");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Integer**|  | [optional] [default to 10] [enum: ]
 **offset** | **Integer**|  | [optional] [enum: ]
 **manga** | [**List&lt;UUID&gt;**](UUID.md)| Manga ids (limited to 100 per request) | [optional]
 **ids** | [**List&lt;UUID&gt;**](UUID.md)| Covers ids (limited to 100 per request) | [optional]
 **uploaders** | [**List&lt;UUID&gt;**](UUID.md)| User ids (limited to 100 per request) | [optional]
 **locales** | [**List&lt;String&gt;**](String.md)| Locales of cover art (limited to 100 per request) | [optional]
 **orderCreatedAt** | **String**|  | [optional] [enum: asc, desc]
 **orderUpdatedAt** | **String**|  | [optional] [enum: asc, desc]
 **orderVolume** | **String**|  | [optional] [enum: asc, desc]
 **includes** | [**List&lt;String&gt;**](String.md)|  | [optional]

### Return type

[**CoverList**](CoverList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getCoverId"></a>
# **getCoverId**
> CoverResponse getCoverId(mangaOrCoverId, includes)

Get Cover

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.CoverApi;


CoverApi apiInstance = new CoverApi();
UUID mangaOrCoverId = new UUID(); // UUID | Is Manga UUID on POST
List<String> includes = Arrays.asList("includes_example"); // List<String> | 
try {
    CoverResponse result = apiInstance.getCoverId(mangaOrCoverId, includes);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoverApi#getCoverId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mangaOrCoverId** | [**UUID**](.md)| Is Manga UUID on POST |
 **includes** | [**List&lt;String&gt;**](String.md)|  | [optional]

### Return type

[**CoverResponse**](CoverResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="uploadCover"></a>
# **uploadCover**
> CoverResponse uploadCover(contentType, mangaOrCoverId, file, volume, description, locale)

Upload Cover

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CoverApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


CoverApi apiInstance = new CoverApi();
String contentType = "multipart/form-data"; // String | 
UUID mangaOrCoverId = new UUID(); // UUID | Is Manga UUID on POST
File file = new File("file_example"); // File | 
String volume = "volume_example"; // String | 
String description = "description_example"; // String | 
String locale = "locale_example"; // String | 
try {
    CoverResponse result = apiInstance.uploadCover(contentType, mangaOrCoverId, file, volume, description, locale);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoverApi#uploadCover");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to multipart/form-data]
 **mangaOrCoverId** | [**UUID**](.md)| Is Manga UUID on POST |
 **file** | **File**|  | [optional]
 **volume** | **String**|  | [optional]
 **description** | **String**|  | [optional]
 **locale** | **String**|  | [optional]

### Return type

[**CoverResponse**](CoverResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

