# BeginUploadSession

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groups** | [**List&lt;UUID&gt;**](UUID.md) |  | 
**manga** | [**UUID**](UUID.md) |  | 
