# MangaApi

All URIs are relative to *https://api.mangadex.org*

Method | HTTP request | Description
------------- | ------------- | -------------
[**commitMangaDraft**](MangaApi.md#commitMangaDraft) | **POST** manga/draft/{id}/commit | Submit a Manga Draft
[**deleteMangaId**](MangaApi.md#deleteMangaId) | **DELETE** manga/{id} | Delete Manga
[**deleteMangaIdFollow**](MangaApi.md#deleteMangaIdFollow) | **DELETE** manga/{id}/follow | Unfollow Manga
[**deleteMangaRelationId**](MangaApi.md#deleteMangaRelationId) | **DELETE** manga/{mangaId}/relation/{id} | Delete Manga relation
[**getMangaDrafts**](MangaApi.md#getMangaDrafts) | **GET** manga/draft | Get a list of Manga Drafts
[**getMangaId**](MangaApi.md#getMangaId) | **GET** manga/{id} | Get Manga
[**getMangaIdDraft**](MangaApi.md#getMangaIdDraft) | **GET** manga/draft/{id} | Get a specific Manga Draft
[**getMangaIdFeed**](MangaApi.md#getMangaIdFeed) | **GET** manga/{id}/feed | Manga feed
[**getMangaIdStatus**](MangaApi.md#getMangaIdStatus) | **GET** manga/{id}/status | Get a Manga reading status
[**getMangaRandom**](MangaApi.md#getMangaRandom) | **GET** manga/random | Get a random Manga
[**getMangaRelation**](MangaApi.md#getMangaRelation) | **GET** manga/{mangaId}/relation | Manga relation list
[**getMangaStatus**](MangaApi.md#getMangaStatus) | **GET** manga/status | Get all Manga reading status for logged User
[**getMangaTag**](MangaApi.md#getMangaTag) | **GET** manga/tag | Tag list
[**getSearchManga**](MangaApi.md#getSearchManga) | **GET** manga | Manga list
[**mangaIdAggregateGet**](MangaApi.md#mangaIdAggregateGet) | **GET** manga/{id}/aggregate | Get Manga volumes &amp; chapters
[**postManga**](MangaApi.md#postManga) | **POST** manga | Create Manga
[**postMangaIdFollow**](MangaApi.md#postMangaIdFollow) | **POST** manga/{id}/follow | Follow Manga
[**postMangaIdStatus**](MangaApi.md#postMangaIdStatus) | **POST** manga/{id}/status | Update Manga reading status
[**postMangaRelation**](MangaApi.md#postMangaRelation) | **POST** manga/{mangaId}/relation | Create Manga relation
[**putMangaId**](MangaApi.md#putMangaId) | **PUT** manga/{id} | Update Manga

<a name="commitMangaDraft"></a>
# **commitMangaDraft**
> MangaResponse commitMangaDraft(id, body)

Submit a Manga Draft

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MangaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


MangaApi apiInstance = new MangaApi();
UUID id = new UUID(); // UUID | 
IdCommitBody body = new IdCommitBody(); // IdCommitBody | A Manga Draft that is to be submitted must have at least one cover in the original language, must be in the "draft" state and must be passed the correct version in the request body.
try {
    MangaResponse result = apiInstance.commitMangaDraft(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MangaApi#commitMangaDraft");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)|  |
 **body** | [**IdCommitBody**](IdCommitBody.md)| A Manga Draft that is to be submitted must have at least one cover in the original language, must be in the &quot;draft&quot; state and must be passed the correct version in the request body. | [optional]

### Return type

[**MangaResponse**](MangaResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteMangaId"></a>
# **deleteMangaId**
> Response deleteMangaId(id)

Delete Manga

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MangaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


MangaApi apiInstance = new MangaApi();
UUID id = new UUID(); // UUID | Manga ID
try {
    Response result = apiInstance.deleteMangaId(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MangaApi#deleteMangaId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| Manga ID |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteMangaIdFollow"></a>
# **deleteMangaIdFollow**
> Response deleteMangaIdFollow(id)

Unfollow Manga

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MangaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


MangaApi apiInstance = new MangaApi();
UUID id = new UUID(); // UUID | 
try {
    Response result = apiInstance.deleteMangaIdFollow(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MangaApi#deleteMangaIdFollow");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)|  |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteMangaRelationId"></a>
# **deleteMangaRelationId**
> Response deleteMangaRelationId(mangaId, id)

Delete Manga relation

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MangaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


MangaApi apiInstance = new MangaApi();
UUID mangaId = new UUID(); // UUID | 
UUID id = new UUID(); // UUID | 
try {
    Response result = apiInstance.deleteMangaRelationId(mangaId, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MangaApi#deleteMangaRelationId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mangaId** | [**UUID**](.md)|  |
 **id** | [**UUID**](.md)|  |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getMangaDrafts"></a>
# **getMangaDrafts**
> MangaResponse getMangaDrafts(limit, offset, state, orderTitle, orderYear, orderCreatedAt, orderUpdatedAt, includes)

Get a list of Manga Drafts

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MangaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


MangaApi apiInstance = new MangaApi();
Integer limit = 10; // Integer | 
Integer offset = 56; // Integer | 
String state = "state_example"; // String | 
String orderTitle = "orderTitle_example"; // String | 
String orderYear = "orderYear_example"; // String | 
String orderCreatedAt = "orderCreatedAt_example"; // String | 
String orderUpdatedAt = "orderUpdatedAt_example"; // String | 
List<String> includes = Arrays.asList("includes_example"); // List<String> | 
try {
    MangaResponse result = apiInstance.getMangaDrafts(limit, offset, state, orderTitle, orderYear, orderCreatedAt, orderUpdatedAt, includes);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MangaApi#getMangaDrafts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Integer**|  | [optional] [default to 10] [enum: ]
 **offset** | **Integer**|  | [optional] [enum: ]
 **state** | **String**|  | [optional] [enum: draft, submitted, rejected]
 **orderTitle** | **String**|  | [optional] [enum: asc, desc]
 **orderYear** | **String**|  | [optional] [enum: asc, desc]
 **orderCreatedAt** | **String**|  | [optional] [enum: asc, desc]
 **orderUpdatedAt** | **String**|  | [optional] [enum: asc, desc]
 **includes** | [**List&lt;String&gt;**](String.md)|  | [optional]

### Return type

[**MangaResponse**](MangaResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getMangaId"></a>
# **getMangaId**
> MangaResponse getMangaId(id, includes)

Get Manga

Get Manga.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.MangaApi;


MangaApi apiInstance = new MangaApi();
UUID id = new UUID(); // UUID | Manga ID
List<String> includes = Arrays.asList("includes_example"); // List<String> | 
try {
    MangaResponse result = apiInstance.getMangaId(id, includes);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MangaApi#getMangaId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| Manga ID |
 **includes** | [**List&lt;String&gt;**](String.md)|  | [optional]

### Return type

[**MangaResponse**](MangaResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getMangaIdDraft"></a>
# **getMangaIdDraft**
> MangaResponse getMangaIdDraft(id, includes)

Get a specific Manga Draft

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MangaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


MangaApi apiInstance = new MangaApi();
UUID id = new UUID(); // UUID | 
List<String> includes = Arrays.asList("includes_example"); // List<String> | 
try {
    MangaResponse result = apiInstance.getMangaIdDraft(id, includes);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MangaApi#getMangaIdDraft");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)|  |
 **includes** | [**List&lt;String&gt;**](String.md)|  | [optional]

### Return type

[**MangaResponse**](MangaResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getMangaIdFeed"></a>
# **getMangaIdFeed**
> ChapterList getMangaIdFeed(id, limit, offset, translatedLanguage, originalLanguage, excludedOriginalLanguage, contentRating, excludedGroups, excludedUploaders, includeFutureUpdates, createdAtSince, updatedAtSince, publishAtSince, orderCreatedAt, orderUpdatedAt, orderPublishAt, orderReadableAt, orderVolume, orderChapter, includes, includeEmptyPages, includeFuturePublishAt, includeExternalUrl)

Manga feed

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.MangaApi;


MangaApi apiInstance = new MangaApi();
UUID id = new UUID(); // UUID | Manga ID
Integer limit = 100; // Integer | 
Integer offset = 56; // Integer | 
List<String> translatedLanguage = Arrays.asList("translatedLanguage_example"); // List<String> | 
List<String> originalLanguage = Arrays.asList("originalLanguage_example"); // List<String> | 
List<String> excludedOriginalLanguage = Arrays.asList("excludedOriginalLanguage_example"); // List<String> | 
List<String> contentRating = Arrays.asList("[\"safe\",\"suggestive\",\"erotica\"]"); // List<String> | 
List<UUID> excludedGroups = Arrays.asList(new UUID()); // List<UUID> | 
List<UUID> excludedUploaders = Arrays.asList(new UUID()); // List<UUID> | 
String includeFutureUpdates = "1"; // String | 
String createdAtSince = "createdAtSince_example"; // String | 
String updatedAtSince = "updatedAtSince_example"; // String | 
String publishAtSince = "publishAtSince_example"; // String | 
String orderCreatedAt = "orderCreatedAt_example"; // String | 
String orderUpdatedAt = "orderUpdatedAt_example"; // String | 
String orderPublishAt = "orderPublishAt_example"; // String | 
String orderReadableAt = "orderReadableAt_example"; // String | 
String orderVolume = "orderVolume_example"; // String | 
String orderChapter = "orderChapter_example"; // String | 
List<String> includes = Arrays.asList("includes_example"); // List<String> | 
Integer includeEmptyPages = 56; // Integer | 
Integer includeFuturePublishAt = 56; // Integer | 
Integer includeExternalUrl = 56; // Integer | 
try {
    ChapterList result = apiInstance.getMangaIdFeed(id, limit, offset, translatedLanguage, originalLanguage, excludedOriginalLanguage, contentRating, excludedGroups, excludedUploaders, includeFutureUpdates, createdAtSince, updatedAtSince, publishAtSince, orderCreatedAt, orderUpdatedAt, orderPublishAt, orderReadableAt, orderVolume, orderChapter, includes, includeEmptyPages, includeFuturePublishAt, includeExternalUrl);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MangaApi#getMangaIdFeed");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| Manga ID |
 **limit** | **Integer**|  | [optional] [default to 100] [enum: ]
 **offset** | **Integer**|  | [optional] [enum: ]
 **translatedLanguage** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **originalLanguage** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **excludedOriginalLanguage** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **contentRating** | [**List&lt;String&gt;**](String.md)|  | [optional] [default to [&quot;safe&quot;,&quot;suggestive&quot;,&quot;erotica&quot;]] [enum: safe, suggestive, erotica, pornographic]
 **excludedGroups** | [**List&lt;UUID&gt;**](UUID.md)|  | [optional]
 **excludedUploaders** | [**List&lt;UUID&gt;**](UUID.md)|  | [optional]
 **includeFutureUpdates** | **String**|  | [optional] [default to 1] [enum: 0, 1]
 **createdAtSince** | **String**|  | [optional]
 **updatedAtSince** | **String**|  | [optional]
 **publishAtSince** | **String**|  | [optional]
 **orderCreatedAt** | **String**|  | [optional] [enum: asc, desc]
 **orderUpdatedAt** | **String**|  | [optional] [enum: asc, desc]
 **orderPublishAt** | **String**|  | [optional] [enum: asc, desc]
 **orderReadableAt** | **String**|  | [optional] [enum: asc, desc]
 **orderVolume** | **String**|  | [optional] [enum: asc, desc]
 **orderChapter** | **String**|  | [optional] [enum: asc, desc]
 **includes** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **includeEmptyPages** | **Integer**|  | [optional] [enum: 0, 1]
 **includeFuturePublishAt** | **Integer**|  | [optional] [enum: 0, 1]
 **includeExternalUrl** | **Integer**|  | [optional] [enum: 0, 1]

### Return type

[**ChapterList**](ChapterList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getMangaIdStatus"></a>
# **getMangaIdStatus**
> InlineResponse2007 getMangaIdStatus(id)

Get a Manga reading status

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MangaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


MangaApi apiInstance = new MangaApi();
UUID id = new UUID(); // UUID | 
try {
    InlineResponse2007 result = apiInstance.getMangaIdStatus(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MangaApi#getMangaIdStatus");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)|  |

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getMangaRandom"></a>
# **getMangaRandom**
> MangaResponse getMangaRandom(includes, contentRating, includedTags, includedTagsMode, excludedTags, excludedTagsMode)

Get a random Manga

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.MangaApi;


MangaApi apiInstance = new MangaApi();
List<String> includes = Arrays.asList("includes_example"); // List<String> | 
List<String> contentRating = Arrays.asList("[\"safe\",\"suggestive\",\"erotica\"]"); // List<String> | 
List<UUID> includedTags = Arrays.asList(new UUID()); // List<UUID> | 
String includedTagsMode = "AND"; // String | 
List<UUID> excludedTags = Arrays.asList(new UUID()); // List<UUID> | 
String excludedTagsMode = "OR"; // String | 
try {
    MangaResponse result = apiInstance.getMangaRandom(includes, contentRating, includedTags, includedTagsMode, excludedTags, excludedTagsMode);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MangaApi#getMangaRandom");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **includes** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **contentRating** | [**List&lt;String&gt;**](String.md)|  | [optional] [default to [&quot;safe&quot;,&quot;suggestive&quot;,&quot;erotica&quot;]] [enum: safe, suggestive, erotica, pornographic]
 **includedTags** | [**List&lt;UUID&gt;**](UUID.md)|  | [optional]
 **includedTagsMode** | **String**|  | [optional] [default to AND] [enum: AND, OR]
 **excludedTags** | [**List&lt;UUID&gt;**](UUID.md)|  | [optional]
 **excludedTagsMode** | **String**|  | [optional] [default to OR] [enum: AND, OR]

### Return type

[**MangaResponse**](MangaResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getMangaRelation"></a>
# **getMangaRelation**
> MangaRelationList getMangaRelation(mangaId, includes)

Manga relation list

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.MangaApi;


MangaApi apiInstance = new MangaApi();
UUID mangaId = new UUID(); // UUID | 
List<String> includes = Arrays.asList("includes_example"); // List<String> | 
try {
    MangaRelationList result = apiInstance.getMangaRelation(mangaId, includes);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MangaApi#getMangaRelation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mangaId** | [**UUID**](.md)|  |
 **includes** | [**List&lt;String&gt;**](String.md)|  | [optional]

### Return type

[**MangaRelationList**](MangaRelationList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getMangaStatus"></a>
# **getMangaStatus**
> InlineResponse2006 getMangaStatus(status)

Get all Manga reading status for logged User

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MangaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


MangaApi apiInstance = new MangaApi();
String status = "status_example"; // String | Used to filter the list by given status
try {
    InlineResponse2006 result = apiInstance.getMangaStatus(status);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MangaApi#getMangaStatus");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **status** | **String**| Used to filter the list by given status | [optional] [enum: reading, on_hold, plan_to_read, dropped, re_reading, completed]

### Return type

[**InlineResponse2006**](InlineResponse2006.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getMangaTag"></a>
# **getMangaTag**
> TagResponse getMangaTag()

Tag list

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.MangaApi;


MangaApi apiInstance = new MangaApi();
try {
    TagResponse result = apiInstance.getMangaTag();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MangaApi#getMangaTag");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**TagResponse**](TagResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getSearchManga"></a>
# **getSearchManga**
> MangaList getSearchManga(limit, offset, title, authorOrArtist, authors, artists, year, includedTags, includedTagsMode, excludedTags, excludedTagsMode, status, originalLanguage, excludedOriginalLanguage, availableTranslatedLanguage, publicationDemographic, ids, contentRating, createdAtSince, updatedAtSince, orderTitle, orderYear, orderCreatedAt, orderUpdatedAt, orderLatestUploadedChapter, orderFollowedCount, orderRelevance, orderRating, includes, hasAvailableChapters, group)

Manga list

Search a list of Manga.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.MangaApi;


MangaApi apiInstance = new MangaApi();
Integer limit = 10; // Integer | 
Integer offset = 56; // Integer | 
String title = "title_example"; // String | 
UUID authorOrArtist = new UUID(); // UUID | 
List<UUID> authors = Arrays.asList(new UUID()); // List<UUID> | 
List<UUID> artists = Arrays.asList(new UUID()); // List<UUID> | 
Integer year = 56; // Integer | Year of release
List<UUID> includedTags = Arrays.asList(new UUID()); // List<UUID> | 
String includedTagsMode = "AND"; // String | 
List<UUID> excludedTags = Arrays.asList(new UUID()); // List<UUID> | 
String excludedTagsMode = "OR"; // String | 
List<String> status = Arrays.asList("status_example"); // List<String> | 
List<String> originalLanguage = Arrays.asList("originalLanguage_example"); // List<String> | 
List<String> excludedOriginalLanguage = Arrays.asList("excludedOriginalLanguage_example"); // List<String> | 
List<String> availableTranslatedLanguage = Arrays.asList("availableTranslatedLanguage_example"); // List<String> | 
List<String> publicationDemographic = Arrays.asList("publicationDemographic_example"); // List<String> | 
List<UUID> ids = Arrays.asList(new UUID()); // List<UUID> | Manga ids (limited to 100 per request)
List<String> contentRating = Arrays.asList("[\"safe\",\"suggestive\",\"erotica\"]"); // List<String> | 
String createdAtSince = "createdAtSince_example"; // String | 
String updatedAtSince = "updatedAtSince_example"; // String | 
String orderTitle = "orderTitle_example"; // String | 
String orderYear = "orderYear_example"; // String | 
String orderCreatedAt = "orderCreatedAt_example"; // String | 
String orderUpdatedAt = "orderUpdatedAt_example"; // String | 
String orderLatestUploadedChapter = "orderLatestUploadedChapter_example"; // String | 
String orderFollowedCount = "orderFollowedCount_example"; // String | 
String orderRelevance = "orderRelevance_example"; // String | 
String orderRating = "orderRating_example"; // String | 
List<String> includes = Arrays.asList("includes_example"); // List<String> | 
String hasAvailableChapters = "hasAvailableChapters_example"; // String | 
UUID group = new UUID(); // UUID | 
try {
    MangaList result = apiInstance.getSearchManga(limit, offset, title, authorOrArtist, authors, artists, year, includedTags, includedTagsMode, excludedTags, excludedTagsMode, status, originalLanguage, excludedOriginalLanguage, availableTranslatedLanguage, publicationDemographic, ids, contentRating, createdAtSince, updatedAtSince, orderTitle, orderYear, orderCreatedAt, orderUpdatedAt, orderLatestUploadedChapter, orderFollowedCount, orderRelevance, orderRating, includes, hasAvailableChapters, group);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MangaApi#getSearchManga");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Integer**|  | [optional] [default to 10] [enum: ]
 **offset** | **Integer**|  | [optional] [enum: ]
 **title** | **String**|  | [optional]
 **authorOrArtist** | [**UUID**](.md)|  | [optional]
 **authors** | [**List&lt;UUID&gt;**](UUID.md)|  | [optional]
 **artists** | [**List&lt;UUID&gt;**](UUID.md)|  | [optional]
 **year** | **Integer**| Year of release | [optional]
 **includedTags** | [**List&lt;UUID&gt;**](UUID.md)|  | [optional]
 **includedTagsMode** | **String**|  | [optional] [default to AND] [enum: AND, OR]
 **excludedTags** | [**List&lt;UUID&gt;**](UUID.md)|  | [optional]
 **excludedTagsMode** | **String**|  | [optional] [default to OR] [enum: AND, OR]
 **status** | [**List&lt;String&gt;**](String.md)|  | [optional] [enum: ongoing, completed, hiatus, cancelled]
 **originalLanguage** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **excludedOriginalLanguage** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **availableTranslatedLanguage** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **publicationDemographic** | [**List&lt;String&gt;**](String.md)|  | [optional] [enum: shounen, shoujo, josei, seinen, none]
 **ids** | [**List&lt;UUID&gt;**](UUID.md)| Manga ids (limited to 100 per request) | [optional]
 **contentRating** | [**List&lt;String&gt;**](String.md)|  | [optional] [default to [&quot;safe&quot;,&quot;suggestive&quot;,&quot;erotica&quot;]] [enum: safe, suggestive, erotica, pornographic]
 **createdAtSince** | **String**|  | [optional]
 **updatedAtSince** | **String**|  | [optional]
 **orderTitle** | **String**|  | [optional] [enum: asc, desc]
 **orderYear** | **String**|  | [optional] [enum: asc, desc]
 **orderCreatedAt** | **String**|  | [optional] [enum: asc, desc]
 **orderUpdatedAt** | **String**|  | [optional] [enum: asc, desc]
 **orderLatestUploadedChapter** | **String**|  | [optional] [enum: asc, desc]
 **orderFollowedCount** | **String**|  | [optional] [enum: asc, desc]
 **orderRelevance** | **String**|  | [optional] [enum: asc, desc]
 **orderRating** | **String**|  | [optional] [enum: asc, desc]
 **includes** | [**List&lt;String&gt;**](String.md)|  | [optional] [enum: manga, chapter, cover_art, author, artist, scanlation_group, tag, user, custom_list]
 **hasAvailableChapters** | **String**|  | [optional] [enum: 0, 1, true, false]
 **group** | [**UUID**](.md)|  | [optional]

### Return type

[**MangaList**](MangaList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="mangaIdAggregateGet"></a>
# **mangaIdAggregateGet**
> InlineResponse200 mangaIdAggregateGet(id, translatedLanguage, groups)

Get Manga volumes &amp; chapters

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.MangaApi;


MangaApi apiInstance = new MangaApi();
UUID id = new UUID(); // UUID | Manga ID
List<String> translatedLanguage = Arrays.asList("translatedLanguage_example"); // List<String> | 
List<UUID> groups = Arrays.asList(new UUID()); // List<UUID> | 
try {
    InlineResponse200 result = apiInstance.mangaIdAggregateGet(id, translatedLanguage, groups);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MangaApi#mangaIdAggregateGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| Manga ID |
 **translatedLanguage** | [**List&lt;String&gt;**](String.md)|  | [optional]
 **groups** | [**List&lt;UUID&gt;**](UUID.md)|  | [optional]

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postManga"></a>
# **postManga**
> MangaResponse postManga(contentType, body)

Create Manga

Create a new Manga.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MangaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


MangaApi apiInstance = new MangaApi();
String contentType = "application/json"; // String | 
MangaCreate body = new MangaCreate(); // MangaCreate | The size of the body is limited to 64KB.
try {
    MangaResponse result = apiInstance.postManga(contentType, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MangaApi#postManga");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **body** | [**MangaCreate**](MangaCreate.md)| The size of the body is limited to 64KB. | [optional]

### Return type

[**MangaResponse**](MangaResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postMangaIdFollow"></a>
# **postMangaIdFollow**
> Response postMangaIdFollow(id)

Follow Manga

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MangaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


MangaApi apiInstance = new MangaApi();
UUID id = new UUID(); // UUID | 
try {
    Response result = apiInstance.postMangaIdFollow(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MangaApi#postMangaIdFollow");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)|  |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postMangaIdStatus"></a>
# **postMangaIdStatus**
> Response postMangaIdStatus(contentType, id, body)

Update Manga reading status

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MangaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


MangaApi apiInstance = new MangaApi();
String contentType = "application/json"; // String | 
UUID id = new UUID(); // UUID | 
UpdateMangaStatus body = new UpdateMangaStatus(); // UpdateMangaStatus | Using a `null` value in `status` field will remove the Manga reading status. The size of the body is limited to 2KB.
try {
    Response result = apiInstance.postMangaIdStatus(contentType, id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MangaApi#postMangaIdStatus");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **id** | [**UUID**](.md)|  |
 **body** | [**UpdateMangaStatus**](UpdateMangaStatus.md)| Using a &#x60;null&#x60; value in &#x60;status&#x60; field will remove the Manga reading status. The size of the body is limited to 2KB. | [optional]

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postMangaRelation"></a>
# **postMangaRelation**
> MangaRelationResponse postMangaRelation(contentType, mangaId, body)

Create Manga relation

Create a new Manga relation.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MangaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


MangaApi apiInstance = new MangaApi();
String contentType = "application/json"; // String | 
UUID mangaId = new UUID(); // UUID | 
MangaRelationCreate body = new MangaRelationCreate(); // MangaRelationCreate | The size of the body is limited to 8KB.
try {
    MangaRelationResponse result = apiInstance.postMangaRelation(contentType, mangaId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MangaApi#postMangaRelation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **mangaId** | [**UUID**](.md)|  |
 **body** | [**MangaRelationCreate**](MangaRelationCreate.md)| The size of the body is limited to 8KB. | [optional]

### Return type

[**MangaRelationResponse**](MangaRelationResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putMangaId"></a>
# **putMangaId**
> MangaResponse putMangaId(contentType, id, body)

Update Manga

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MangaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


MangaApi apiInstance = new MangaApi();
String contentType = "application/json"; // String | 
UUID id = new UUID(); // UUID | Manga ID
MangaIdBody body = new MangaIdBody(); // MangaIdBody | The size of the body is limited to 64KB.
try {
    MangaResponse result = apiInstance.putMangaId(contentType, id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MangaApi#putMangaId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **id** | [**UUID**](.md)| Manga ID |
 **body** | [**MangaIdBody**](MangaIdBody.md)| The size of the body is limited to 64KB. | [optional]

### Return type

[**MangaResponse**](MangaResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

