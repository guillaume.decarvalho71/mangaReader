# ReportApi

All URIs are relative to *https://api.mangadex.org*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getReportReasonsByCategory**](ReportApi.md#getReportReasonsByCategory) | **GET** report/reasons/{category} | Get a list of report reasons
[**getReports**](ReportApi.md#getReports) | **GET** report | Get a list of reports by the user
[**postReport**](ReportApi.md#postReport) | **POST** report | Create a new Report

<a name="getReportReasonsByCategory"></a>
# **getReportReasonsByCategory**
> InlineResponse2009 getReportReasonsByCategory(category)

Get a list of report reasons

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ReportApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


ReportApi apiInstance = new ReportApi();
String category = "category_example"; // String | 
try {
    InlineResponse2009 result = apiInstance.getReportReasonsByCategory(category);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReportApi#getReportReasonsByCategory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category** | **String**|  | [enum: manga, chapter, scanlation_group, user, author]

### Return type

[**InlineResponse2009**](InlineResponse2009.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReports"></a>
# **getReports**
> ReportListResponse getReports(limit, offset, category, reasonId, objectId, status, orderCreatedAt, includes)

Get a list of reports by the user

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ReportApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


ReportApi apiInstance = new ReportApi();
Integer limit = 10; // Integer | 
Integer offset = 56; // Integer | 
String category = "category_example"; // String | 
UUID reasonId = new UUID(); // UUID | 
UUID objectId = new UUID(); // UUID | 
String status = "status_example"; // String | 
String orderCreatedAt = "orderCreatedAt_example"; // String | 
List<String> includes = Arrays.asList("includes_example"); // List<String> | 
try {
    ReportListResponse result = apiInstance.getReports(limit, offset, category, reasonId, objectId, status, orderCreatedAt, includes);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReportApi#getReports");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Integer**|  | [optional] [default to 10] [enum: ]
 **offset** | **Integer**|  | [optional] [enum: ]
 **category** | **String**|  | [optional] [enum: manga, chapter, scanlation_group, user, author]
 **reasonId** | [**UUID**](.md)|  | [optional]
 **objectId** | [**UUID**](.md)|  | [optional]
 **status** | **String**|  | [optional] [enum: waiting, accepted, refused, autoresolved]
 **orderCreatedAt** | **String**|  | [optional] [enum: asc, desc]
 **includes** | [**List&lt;String&gt;**](String.md)|  | [optional]

### Return type

[**ReportListResponse**](ReportListResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postReport"></a>
# **postReport**
> Response postReport(contentType, body)

Create a new Report

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ReportApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


ReportApi apiInstance = new ReportApi();
String contentType = "application/json"; // String | 
ReportBody body = new ReportBody(); // ReportBody | The size of the body is limited to 8KB.
try {
    Response result = apiInstance.postReport(contentType, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ReportApi#postReport");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **body** | [**ReportBody**](ReportBody.md)| The size of the body is limited to 8KB. | [optional]

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

