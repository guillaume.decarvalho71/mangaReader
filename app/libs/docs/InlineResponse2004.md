# InlineResponse2004

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**ResultEnum**](#ResultEnum) |  |  [optional]
**data** | **OneOfinlineResponse2004Data** |  |  [optional]

<a name="ResultEnum"></a>
## Enum: ResultEnum
Name | Value
---- | -----
OK | &quot;ok&quot;
