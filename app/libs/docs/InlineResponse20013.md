# InlineResponse20013

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | **String** |  |  [optional]
**statistics** | [**Map&lt;String, InlineResponse20013Statistics&gt;**](InlineResponse20013Statistics.md) |  |  [optional]
