# UploadSessionFileAttributes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**originalFileName** | **String** |  |  [optional]
**fileHash** | **String** |  |  [optional]
**fileSize** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**mimeType** | **String** |  |  [optional]
**source** | [**SourceEnum**](#SourceEnum) |  |  [optional]
**version** | **Integer** |  |  [optional]

<a name="SourceEnum"></a>
## Enum: SourceEnum
Name | Value
---- | -----
LOCAL | &quot;local&quot;
REMOTE | &quot;remote&quot;
