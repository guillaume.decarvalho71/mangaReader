# FollowsApi

All URIs are relative to *https://api.mangadex.org*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getUserFollowsGroup**](FollowsApi.md#getUserFollowsGroup) | **GET** user/follows/group | Get logged User followed Groups
[**getUserFollowsGroupId**](FollowsApi.md#getUserFollowsGroupId) | **GET** user/follows/group/{id} | Check if logged User follows a Group
[**getUserFollowsList**](FollowsApi.md#getUserFollowsList) | **GET** user/follows/list | Get logged User followed CustomList list
[**getUserFollowsListId**](FollowsApi.md#getUserFollowsListId) | **GET** user/follows/list/{id} | Check if logged User follows a CustomList
[**getUserFollowsManga**](FollowsApi.md#getUserFollowsManga) | **GET** user/follows/manga | Get logged User followed Manga list
[**getUserFollowsMangaId**](FollowsApi.md#getUserFollowsMangaId) | **GET** user/follows/manga/{id} | Check if logged User follows a Manga
[**getUserFollowsUser**](FollowsApi.md#getUserFollowsUser) | **GET** user/follows/user | Get logged User followed User list
[**getUserFollowsUserId**](FollowsApi.md#getUserFollowsUserId) | **GET** user/follows/user/{id} | Check if logged User follows a User

<a name="getUserFollowsGroup"></a>
# **getUserFollowsGroup**
> ScanlationGroupList getUserFollowsGroup(limit, offset, includes)

Get logged User followed Groups

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FollowsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


FollowsApi apiInstance = new FollowsApi();
Integer limit = 10; // Integer | 
Integer offset = 56; // Integer | 
List<String> includes = Arrays.asList("includes_example"); // List<String> | 
try {
    ScanlationGroupList result = apiInstance.getUserFollowsGroup(limit, offset, includes);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FollowsApi#getUserFollowsGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Integer**|  | [optional] [default to 10] [enum: ]
 **offset** | **Integer**|  | [optional] [enum: ]
 **includes** | [**List&lt;String&gt;**](String.md)|  | [optional]

### Return type

[**ScanlationGroupList**](ScanlationGroupList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserFollowsGroupId"></a>
# **getUserFollowsGroupId**
> Response getUserFollowsGroupId(id)

Check if logged User follows a Group

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FollowsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


FollowsApi apiInstance = new FollowsApi();
UUID id = new UUID(); // UUID | Scanlation Group id
try {
    Response result = apiInstance.getUserFollowsGroupId(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FollowsApi#getUserFollowsGroupId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| Scanlation Group id |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserFollowsList"></a>
# **getUserFollowsList**
> CustomListList getUserFollowsList(limit, offset)

Get logged User followed CustomList list

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FollowsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


FollowsApi apiInstance = new FollowsApi();
Integer limit = 10; // Integer | 
Integer offset = 56; // Integer | 
try {
    CustomListList result = apiInstance.getUserFollowsList(limit, offset);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FollowsApi#getUserFollowsList");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Integer**|  | [optional] [default to 10] [enum: ]
 **offset** | **Integer**|  | [optional] [enum: ]

### Return type

[**CustomListList**](CustomListList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserFollowsListId"></a>
# **getUserFollowsListId**
> Response getUserFollowsListId(id)

Check if logged User follows a CustomList

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FollowsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


FollowsApi apiInstance = new FollowsApi();
UUID id = new UUID(); // UUID | CustomList id
try {
    Response result = apiInstance.getUserFollowsListId(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FollowsApi#getUserFollowsListId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| CustomList id |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserFollowsManga"></a>
# **getUserFollowsManga**
> MangaList getUserFollowsManga(limit, offset, includes)

Get logged User followed Manga list

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FollowsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


FollowsApi apiInstance = new FollowsApi();
Integer limit = 10; // Integer | 
Integer offset = 56; // Integer | 
List<String> includes = Arrays.asList("includes_example"); // List<String> | 
try {
    MangaList result = apiInstance.getUserFollowsManga(limit, offset, includes);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FollowsApi#getUserFollowsManga");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Integer**|  | [optional] [default to 10] [enum: ]
 **offset** | **Integer**|  | [optional] [enum: ]
 **includes** | [**List&lt;String&gt;**](String.md)|  | [optional]

### Return type

[**MangaList**](MangaList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserFollowsMangaId"></a>
# **getUserFollowsMangaId**
> Response getUserFollowsMangaId(id)

Check if logged User follows a Manga

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FollowsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


FollowsApi apiInstance = new FollowsApi();
UUID id = new UUID(); // UUID | Manga id
try {
    Response result = apiInstance.getUserFollowsMangaId(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FollowsApi#getUserFollowsMangaId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| Manga id |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserFollowsUser"></a>
# **getUserFollowsUser**
> UserList getUserFollowsUser(limit, offset)

Get logged User followed User list

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FollowsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


FollowsApi apiInstance = new FollowsApi();
Integer limit = 10; // Integer | 
Integer offset = 56; // Integer | 
try {
    UserList result = apiInstance.getUserFollowsUser(limit, offset);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FollowsApi#getUserFollowsUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Integer**|  | [optional] [default to 10] [enum: ]
 **offset** | **Integer**|  | [optional] [enum: ]

### Return type

[**UserList**](UserList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserFollowsUserId"></a>
# **getUserFollowsUserId**
> Response getUserFollowsUserId(id)

Check if logged User follows a User

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.FollowsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


FollowsApi apiInstance = new FollowsApi();
UUID id = new UUID(); // UUID | User id
try {
    Response result = apiInstance.getUserFollowsUserId(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FollowsApi#getUserFollowsUserId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| User id |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

