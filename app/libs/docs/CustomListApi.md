# CustomListApi

All URIs are relative to *https://api.mangadex.org*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteListId**](CustomListApi.md#deleteListId) | **DELETE** list/{id} | Delete CustomList
[**deleteMangaIdListListId**](CustomListApi.md#deleteMangaIdListListId) | **DELETE** manga/{id}/list/{listId} | Remove Manga in CustomList
[**followListId**](CustomListApi.md#followListId) | **POST** list/{id}/follow | Follow CustomList
[**getListId**](CustomListApi.md#getListId) | **GET** list/{id} | Get CustomList
[**getUserIdList**](CustomListApi.md#getUserIdList) | **GET** user/{id}/list | Get User&#x27;s CustomList list
[**getUserList**](CustomListApi.md#getUserList) | **GET** user/list | Get logged User CustomList list
[**postList**](CustomListApi.md#postList) | **POST** list | Create CustomList
[**postMangaIdListListId**](CustomListApi.md#postMangaIdListListId) | **POST** manga/{id}/list/{listId} | Add Manga in CustomList
[**putListId**](CustomListApi.md#putListId) | **PUT** list/{id} | Update CustomList
[**unfollowListId**](CustomListApi.md#unfollowListId) | **DELETE** list/{id}/follow | Unfollow CustomList

<a name="deleteListId"></a>
# **deleteListId**
> Response deleteListId(id)

Delete CustomList

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomListApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


CustomListApi apiInstance = new CustomListApi();
UUID id = new UUID(); // UUID | CustomList ID
try {
    Response result = apiInstance.deleteListId(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomListApi#deleteListId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| CustomList ID |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteMangaIdListListId"></a>
# **deleteMangaIdListListId**
> Response deleteMangaIdListListId(id, listId)

Remove Manga in CustomList

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomListApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


CustomListApi apiInstance = new CustomListApi();
UUID id = new UUID(); // UUID | Manga ID
UUID listId = new UUID(); // UUID | CustomList ID
try {
    Response result = apiInstance.deleteMangaIdListListId(id, listId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomListApi#deleteMangaIdListListId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| Manga ID |
 **listId** | [**UUID**](.md)| CustomList ID |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="followListId"></a>
# **followListId**
> InlineResponse2002 followListId(contentType, id, body)

Follow CustomList

The request body is empty

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomListApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


CustomListApi apiInstance = new CustomListApi();
String contentType = "application/json"; // String | 
UUID id = new UUID(); // UUID | CustomList ID
Object body = null; // Object | 
try {
    InlineResponse2002 result = apiInstance.followListId(contentType, id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomListApi#followListId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **id** | [**UUID**](.md)| CustomList ID |
 **body** | [**Object**](Object.md)|  | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getListId"></a>
# **getListId**
> CustomListResponse getListId(id)

Get CustomList

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.CustomListApi;


CustomListApi apiInstance = new CustomListApi();
UUID id = new UUID(); // UUID | CustomList ID
try {
    CustomListResponse result = apiInstance.getListId(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomListApi#getListId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| CustomList ID |

### Return type

[**CustomListResponse**](CustomListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserIdList"></a>
# **getUserIdList**
> CustomListList getUserIdList(id, limit, offset)

Get User&#x27;s CustomList list

This will list only public CustomList

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.CustomListApi;


CustomListApi apiInstance = new CustomListApi();
UUID id = new UUID(); // UUID | User ID
Integer limit = 10; // Integer | 
Integer offset = 56; // Integer | 
try {
    CustomListList result = apiInstance.getUserIdList(id, limit, offset);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomListApi#getUserIdList");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| User ID |
 **limit** | **Integer**|  | [optional] [default to 10] [enum: ]
 **offset** | **Integer**|  | [optional] [enum: ]

### Return type

[**CustomListList**](CustomListList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserList"></a>
# **getUserList**
> CustomListList getUserList(limit, offset)

Get logged User CustomList list

This will list public and private CustomList

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomListApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


CustomListApi apiInstance = new CustomListApi();
Integer limit = 10; // Integer | 
Integer offset = 56; // Integer | 
try {
    CustomListList result = apiInstance.getUserList(limit, offset);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomListApi#getUserList");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Integer**|  | [optional] [default to 10] [enum: ]
 **offset** | **Integer**|  | [optional] [enum: ]

### Return type

[**CustomListList**](CustomListList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postList"></a>
# **postList**
> CustomListResponse postList(contentType, body)

Create CustomList

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomListApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


CustomListApi apiInstance = new CustomListApi();
String contentType = "application/json"; // String | 
CustomListCreate body = new CustomListCreate(); // CustomListCreate | The size of the body is limited to 8KB.
try {
    CustomListResponse result = apiInstance.postList(contentType, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomListApi#postList");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **body** | [**CustomListCreate**](CustomListCreate.md)| The size of the body is limited to 8KB. | [optional]

### Return type

[**CustomListResponse**](CustomListResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postMangaIdListListId"></a>
# **postMangaIdListListId**
> Response postMangaIdListListId(id, listId)

Add Manga in CustomList

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomListApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


CustomListApi apiInstance = new CustomListApi();
UUID id = new UUID(); // UUID | Manga ID
UUID listId = new UUID(); // UUID | CustomList ID
try {
    Response result = apiInstance.postMangaIdListListId(id, listId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomListApi#postMangaIdListListId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| Manga ID |
 **listId** | [**UUID**](.md)| CustomList ID |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="putListId"></a>
# **putListId**
> CustomListResponse putListId(contentType, id, body)

Update CustomList

The size of the body is limited to 8KB.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomListApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


CustomListApi apiInstance = new CustomListApi();
String contentType = "application/json"; // String | 
UUID id = new UUID(); // UUID | CustomList ID
CustomListEdit body = new CustomListEdit(); // CustomListEdit | 
try {
    CustomListResponse result = apiInstance.putListId(contentType, id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomListApi#putListId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **id** | [**UUID**](.md)| CustomList ID |
 **body** | [**CustomListEdit**](CustomListEdit.md)|  | [optional]

### Return type

[**CustomListResponse**](CustomListResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="unfollowListId"></a>
# **unfollowListId**
> InlineResponse2002 unfollowListId(id, body)

Unfollow CustomList

The request body is empty

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CustomListApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


CustomListApi apiInstance = new CustomListApi();
UUID id = new UUID(); // UUID | CustomList ID
Object body = null; // Object | 
try {
    InlineResponse2002 result = apiInstance.unfollowListId(id, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomListApi#unfollowListId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| CustomList ID |
 **body** | [**Object**](Object.md)|  | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

