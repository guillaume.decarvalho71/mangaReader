# CoverMangaOrCoverIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | [**File**](File.md) |  |  [optional]
**volume** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**locale** | **String** |  |  [optional]
