# Order

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | [**TitleEnum**](#TitleEnum) |  |  [optional]
**year** | [**YearEnum**](#YearEnum) |  |  [optional]
**createdAt** | [**CreatedAtEnum**](#CreatedAtEnum) |  |  [optional]
**updatedAt** | [**UpdatedAtEnum**](#UpdatedAtEnum) |  |  [optional]
**latestUploadedChapter** | [**LatestUploadedChapterEnum**](#LatestUploadedChapterEnum) |  |  [optional]
**followedCount** | [**FollowedCountEnum**](#FollowedCountEnum) |  |  [optional]
**relevance** | [**RelevanceEnum**](#RelevanceEnum) |  |  [optional]
**rating** | [**RatingEnum**](#RatingEnum) |  |  [optional]

<a name="TitleEnum"></a>
## Enum: TitleEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="YearEnum"></a>
## Enum: YearEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="CreatedAtEnum"></a>
## Enum: CreatedAtEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="UpdatedAtEnum"></a>
## Enum: UpdatedAtEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="LatestUploadedChapterEnum"></a>
## Enum: LatestUploadedChapterEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="FollowedCountEnum"></a>
## Enum: FollowedCountEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="RelevanceEnum"></a>
## Enum: RelevanceEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="RatingEnum"></a>
## Enum: RatingEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;
