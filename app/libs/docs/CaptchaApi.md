# CaptchaApi

All URIs are relative to *https://api.mangadex.org*

Method | HTTP request | Description
------------- | ------------- | -------------
[**postCaptchaSolve**](CaptchaApi.md#postCaptchaSolve) | **POST** captcha/solve | Solve Captcha

<a name="postCaptchaSolve"></a>
# **postCaptchaSolve**
> InlineResponse2008 postCaptchaSolve(contentType, body)

Solve Captcha

Captchas can be solved explicitly through this endpoint, another way is to add a &#x60;X-Captcha-Result&#x60; header to any request. The same logic will verify the captcha and is probably more convenient because it takes one less request.  Authentication is optional. Captchas are tracked for both the client ip and for the user id, if you are logged in you want to send your session token but that is not required.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.CaptchaApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


CaptchaApi apiInstance = new CaptchaApi();
String contentType = "application/json"; // String | 
CaptchaSolveBody body = new CaptchaSolveBody(); // CaptchaSolveBody | 
try {
    InlineResponse2008 result = apiInstance.postCaptchaSolve(contentType, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CaptchaApi#postCaptchaSolve");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **body** | [**CaptchaSolveBody**](CaptchaSolveBody.md)|  | [optional]

### Return type

[**InlineResponse2008**](InlineResponse2008.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

