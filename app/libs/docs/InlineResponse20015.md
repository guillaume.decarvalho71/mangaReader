# InlineResponse20015

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | **String** |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**settings** | **Object** | Settings that were validated against the linked template |  [optional]
**template** | [**UUID**](UUID.md) | Settings template UUID |  [optional]
