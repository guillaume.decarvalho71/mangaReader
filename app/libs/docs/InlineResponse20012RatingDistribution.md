# InlineResponse20012RatingDistribution

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_1** | **Integer** |  |  [optional]
**_2** | **Integer** |  |  [optional]
**_3** | **Integer** |  |  [optional]
**_4** | **Integer** |  |  [optional]
**_5** | **Integer** |  |  [optional]
**_6** | **Integer** |  |  [optional]
**_7** | **Integer** |  |  [optional]
**_8** | **Integer** |  |  [optional]
**_9** | **Integer** |  |  [optional]
**_10** | **Integer** |  |  [optional]
