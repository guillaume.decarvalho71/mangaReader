# CustomListCreate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**visibility** | [**VisibilityEnum**](#VisibilityEnum) |  |  [optional]
**manga** | [**List&lt;UUID&gt;**](UUID.md) |  |  [optional]
**version** | **Integer** |  |  [optional]

<a name="VisibilityEnum"></a>
## Enum: VisibilityEnum
Name | Value
---- | -----
PUBLIC | &quot;public&quot;
PRIVATE | &quot;private&quot;
