# MangaRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | [**LocalizedString**](LocalizedString.md) |  |  [optional]
**altTitles** | [**List&lt;LocalizedString&gt;**](LocalizedString.md) |  |  [optional]
**description** | [**LocalizedString**](LocalizedString.md) |  |  [optional]
**authors** | [**List&lt;UUID&gt;**](UUID.md) |  |  [optional]
**artists** | [**List&lt;UUID&gt;**](UUID.md) |  |  [optional]
**links** | **Map&lt;String, String&gt;** |  |  [optional]
**originalLanguage** | **String** |  |  [optional]
**lastVolume** | **String** |  |  [optional]
**lastChapter** | **String** |  |  [optional]
**publicationDemographic** | [**PublicationDemographicEnum**](#PublicationDemographicEnum) |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]
**year** | **Integer** | Year of release |  [optional]
**contentRating** | [**ContentRatingEnum**](#ContentRatingEnum) |  |  [optional]
**chapterNumbersResetOnNewVolume** | **Boolean** |  |  [optional]
**tags** | [**List&lt;UUID&gt;**](UUID.md) |  |  [optional]
**primaryCover** | [**UUID**](UUID.md) |  |  [optional]
**version** | **Integer** |  |  [optional]

<a name="PublicationDemographicEnum"></a>
## Enum: PublicationDemographicEnum
Name | Value
---- | -----
SHOUNEN | &quot;shounen&quot;
SHOUJO | &quot;shoujo&quot;
JOSEI | &quot;josei&quot;
SEINEN | &quot;seinen&quot;

<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
COMPLETED | &quot;completed&quot;
ONGOING | &quot;ongoing&quot;
CANCELLED | &quot;cancelled&quot;
HIATUS | &quot;hiatus&quot;

<a name="ContentRatingEnum"></a>
## Enum: ContentRatingEnum
Name | Value
---- | -----
SAFE | &quot;safe&quot;
SUGGESTIVE | &quot;suggestive&quot;
EROTICA | &quot;erotica&quot;
PORNOGRAPHIC | &quot;pornographic&quot;
