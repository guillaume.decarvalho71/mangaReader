# ReportAttributes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**details** | **String** |  |  [optional]
**objectId** | **String** |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]
**createdAt** | **String** |  |  [optional]

<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
WAITING | &quot;waiting&quot;
ACCEPTED | &quot;accepted&quot;
REFUSED | &quot;refused&quot;
AUTORESOLVED | &quot;autoresolved&quot;
