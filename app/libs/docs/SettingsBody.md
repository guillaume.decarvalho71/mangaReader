# SettingsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**settings** | **Object** | A JSON object that can be validated against the lastest available template |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) | Format: 2022-03-14T13:19:37 |  [optional]
