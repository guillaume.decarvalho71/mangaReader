# Order8

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | [**CreatedAtEnum**](#CreatedAtEnum) |  |  [optional]
**updatedAt** | [**UpdatedAtEnum**](#UpdatedAtEnum) |  |  [optional]
**publishAt** | [**PublishAtEnum**](#PublishAtEnum) |  |  [optional]
**readableAt** | [**ReadableAtEnum**](#ReadableAtEnum) |  |  [optional]
**volume** | [**VolumeEnum**](#VolumeEnum) |  |  [optional]
**chapter** | [**ChapterEnum**](#ChapterEnum) |  |  [optional]

<a name="CreatedAtEnum"></a>
## Enum: CreatedAtEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="UpdatedAtEnum"></a>
## Enum: UpdatedAtEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="PublishAtEnum"></a>
## Enum: PublishAtEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="ReadableAtEnum"></a>
## Enum: ReadableAtEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="VolumeEnum"></a>
## Enum: VolumeEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="ChapterEnum"></a>
## Enum: ChapterEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;
