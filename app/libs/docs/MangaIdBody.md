# MangaIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**artists** | [**List&lt;UUID&gt;**](UUID.md) |  |  [optional]
**authors** | [**List&lt;UUID&gt;**](UUID.md) |  |  [optional]
