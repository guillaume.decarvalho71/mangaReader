# InlineResponse20012Statistics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rating** | [**InlineResponse20012Rating**](InlineResponse20012Rating.md) |  |  [optional]
**follows** | **Integer** |  |  [optional]
