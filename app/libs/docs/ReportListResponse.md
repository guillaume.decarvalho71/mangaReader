# ReportListResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | [**ResultEnum**](#ResultEnum) |  |  [optional]
**response** | **String** |  |  [optional]
**data** | [**List&lt;Report&gt;**](Report.md) |  |  [optional]
**limit** | **Integer** |  |  [optional]
**offset** | **Integer** |  |  [optional]
**total** | **Integer** |  |  [optional]

<a name="ResultEnum"></a>
## Enum: ResultEnum
Name | Value
---- | -----
OK | &quot;ok&quot;
ERROR | &quot;error&quot;
