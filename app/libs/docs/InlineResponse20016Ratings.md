# InlineResponse20016Ratings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chapterId** | **String** |  |  [optional]
**readDate** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
