# MangaAttributes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | [**LocalizedString**](LocalizedString.md) |  |  [optional]
**altTitles** | [**List&lt;LocalizedString&gt;**](LocalizedString.md) |  |  [optional]
**description** | [**LocalizedString**](LocalizedString.md) |  |  [optional]
**isLocked** | **Boolean** |  |  [optional]
**links** | **Map&lt;String, String&gt;** |  |  [optional]
**originalLanguage** | **String** |  |  [optional]
**lastVolume** | **String** |  |  [optional]
**lastChapter** | **String** |  |  [optional]
**publicationDemographic** | [**PublicationDemographicEnum**](#PublicationDemographicEnum) |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]
**year** | **Integer** | Year of release |  [optional]
**contentRating** | [**ContentRatingEnum**](#ContentRatingEnum) |  |  [optional]
**chapterNumbersResetOnNewVolume** | **Boolean** |  |  [optional]
**availableTranslatedLanguages** | [**List**](List.md) |  |  [optional]
**latestUploadedChapter** | [**UUID**](UUID.md) |  |  [optional]
**tags** | [**List&lt;Tag&gt;**](Tag.md) |  |  [optional]
**state** | [**StateEnum**](#StateEnum) |  |  [optional]
**version** | **Integer** |  |  [optional]
**createdAt** | **String** |  |  [optional]
**updatedAt** | **String** |  |  [optional]

<a name="PublicationDemographicEnum"></a>
## Enum: PublicationDemographicEnum
Name | Value
---- | -----
SHOUNEN | &quot;shounen&quot;
SHOUJO | &quot;shoujo&quot;
JOSEI | &quot;josei&quot;
SEINEN | &quot;seinen&quot;

<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
COMPLETED | &quot;completed&quot;
ONGOING | &quot;ongoing&quot;
CANCELLED | &quot;cancelled&quot;
HIATUS | &quot;hiatus&quot;

<a name="ContentRatingEnum"></a>
## Enum: ContentRatingEnum
Name | Value
---- | -----
SAFE | &quot;safe&quot;
SUGGESTIVE | &quot;suggestive&quot;
EROTICA | &quot;erotica&quot;
PORNOGRAPHIC | &quot;pornographic&quot;

<a name="StateEnum"></a>
## Enum: StateEnum
Name | Value
---- | -----
DRAFT | &quot;draft&quot;
SUBMITTED | &quot;submitted&quot;
PUBLISHED | &quot;published&quot;
REJECTED | &quot;rejected&quot;
