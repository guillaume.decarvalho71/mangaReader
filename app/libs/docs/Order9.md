# Order9

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | [**TitleEnum**](#TitleEnum) |  |  [optional]
**year** | [**YearEnum**](#YearEnum) |  |  [optional]
**createdAt** | [**CreatedAtEnum**](#CreatedAtEnum) |  |  [optional]
**updatedAt** | [**UpdatedAtEnum**](#UpdatedAtEnum) |  |  [optional]

<a name="TitleEnum"></a>
## Enum: TitleEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="YearEnum"></a>
## Enum: YearEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="CreatedAtEnum"></a>
## Enum: CreatedAtEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="UpdatedAtEnum"></a>
## Enum: UpdatedAtEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;
