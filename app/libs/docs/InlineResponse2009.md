# InlineResponse2009

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | **String** |  |  [optional]
**response** | **String** |  |  [optional]
**data** | [**List&lt;InlineResponse2009Data&gt;**](InlineResponse2009Data.md) |  |  [optional]
**limit** | **Integer** |  |  [optional]
**offset** | **Integer** |  |  [optional]
**total** | **Integer** |  |  [optional]
