# UploadApi

All URIs are relative to *https://api.mangadex.org*

Method | HTTP request | Description
------------- | ------------- | -------------
[**abandonUploadSession**](UploadApi.md#abandonUploadSession) | **DELETE** upload/{uploadSessionId} | Abandon upload session
[**beginEditSession**](UploadApi.md#beginEditSession) | **POST** upload/begin/{chapterId} | Start an edit chapter session
[**beginUploadSession**](UploadApi.md#beginUploadSession) | **POST** upload/begin | Start an upload session
[**commitUploadSession**](UploadApi.md#commitUploadSession) | **POST** upload/{uploadSessionId}/commit | Commit the upload session and specify chapter data
[**deleteUploadedSessionFile**](UploadApi.md#deleteUploadedSessionFile) | **DELETE** upload/{uploadSessionId}/{uploadSessionFileId} | Delete an uploaded image from the Upload Session
[**deleteUploadedSessionFiles**](UploadApi.md#deleteUploadedSessionFiles) | **DELETE** upload/{uploadSessionId}/batch | Delete a set of uploaded images from the Upload Session
[**getUploadSession**](UploadApi.md#getUploadSession) | **GET** upload | Get the current User upload session
[**putUploadSessionFile**](UploadApi.md#putUploadSessionFile) | **POST** upload/{uploadSessionId} | Upload images to the upload session

<a name="abandonUploadSession"></a>
# **abandonUploadSession**
> Response abandonUploadSession(uploadSessionId)

Abandon upload session

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.UploadApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


UploadApi apiInstance = new UploadApi();
UUID uploadSessionId = new UUID(); // UUID | 
try {
    Response result = apiInstance.abandonUploadSession(uploadSessionId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UploadApi#abandonUploadSession");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uploadSessionId** | [**UUID**](.md)|  |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="beginEditSession"></a>
# **beginEditSession**
> UploadSession beginEditSession(contentType, chapterId, body)

Start an edit chapter session

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.UploadApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


UploadApi apiInstance = new UploadApi();
String contentType = "application/json"; // String | 
UUID chapterId = new UUID(); // UUID | 
BeginEditSession body = new BeginEditSession(); // BeginEditSession | The size of the body is limited to 1KB.
try {
    UploadSession result = apiInstance.beginEditSession(contentType, chapterId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UploadApi#beginEditSession");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **chapterId** | [**UUID**](.md)|  |
 **body** | [**BeginEditSession**](BeginEditSession.md)| The size of the body is limited to 1KB. | [optional]

### Return type

[**UploadSession**](UploadSession.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="beginUploadSession"></a>
# **beginUploadSession**
> UploadSession beginUploadSession(contentType, body)

Start an upload session

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.UploadApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


UploadApi apiInstance = new UploadApi();
String contentType = "application/json"; // String | 
BeginUploadSession body = new BeginUploadSession(); // BeginUploadSession | The size of the body is limited to 4KB.
try {
    UploadSession result = apiInstance.beginUploadSession(contentType, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UploadApi#beginUploadSession");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **body** | [**BeginUploadSession**](BeginUploadSession.md)| The size of the body is limited to 4KB. | [optional]

### Return type

[**UploadSession**](UploadSession.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="commitUploadSession"></a>
# **commitUploadSession**
> Chapter commitUploadSession(contentType, uploadSessionId, body)

Commit the upload session and specify chapter data

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.UploadApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


UploadApi apiInstance = new UploadApi();
String contentType = "application/json"; // String | 
UUID uploadSessionId = new UUID(); // UUID | 
CommitUploadSession body = new CommitUploadSession(); // CommitUploadSession | The size of the body is limited to 4KB.
try {
    Chapter result = apiInstance.commitUploadSession(contentType, uploadSessionId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UploadApi#commitUploadSession");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **uploadSessionId** | [**UUID**](.md)|  |
 **body** | [**CommitUploadSession**](CommitUploadSession.md)| The size of the body is limited to 4KB. | [optional]

### Return type

[**Chapter**](Chapter.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteUploadedSessionFile"></a>
# **deleteUploadedSessionFile**
> Response deleteUploadedSessionFile(uploadSessionId, uploadSessionFileId)

Delete an uploaded image from the Upload Session

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.UploadApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


UploadApi apiInstance = new UploadApi();
UUID uploadSessionId = new UUID(); // UUID | 
UUID uploadSessionFileId = new UUID(); // UUID | 
try {
    Response result = apiInstance.deleteUploadedSessionFile(uploadSessionId, uploadSessionFileId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UploadApi#deleteUploadedSessionFile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uploadSessionId** | [**UUID**](.md)|  |
 **uploadSessionFileId** | [**UUID**](.md)|  |

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteUploadedSessionFiles"></a>
# **deleteUploadedSessionFiles**
> Response deleteUploadedSessionFiles(contentType, uploadSessionId, body)

Delete a set of uploaded images from the Upload Session

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.UploadApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


UploadApi apiInstance = new UploadApi();
String contentType = "application/json"; // String | 
UUID uploadSessionId = new UUID(); // UUID | 
List<UUID> body = Arrays.asList(new UUID()); // List<UUID> | The size of the body is limited to 20KB.
try {
    Response result = apiInstance.deleteUploadedSessionFiles(contentType, uploadSessionId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UploadApi#deleteUploadedSessionFiles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **uploadSessionId** | [**UUID**](.md)|  |
 **body** | [**List&lt;UUID&gt;**](UUID.md)| The size of the body is limited to 20KB. | [optional]

### Return type

[**Response**](Response.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getUploadSession"></a>
# **getUploadSession**
> UploadSession getUploadSession()

Get the current User upload session

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.UploadApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


UploadApi apiInstance = new UploadApi();
try {
    UploadSession result = apiInstance.getUploadSession();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UploadApi#getUploadSession");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UploadSession**](UploadSession.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="putUploadSessionFile"></a>
# **putUploadSessionFile**
> InlineResponse20010 putUploadSessionFile(contentType, uploadSessionId, file)

Upload images to the upload session

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.UploadApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();


UploadApi apiInstance = new UploadApi();
String contentType = "application/json"; // String | 
UUID uploadSessionId = new UUID(); // UUID | 
File file = new File("file_example"); // File | 
try {
    InlineResponse20010 result = apiInstance.putUploadSessionFile(contentType, uploadSessionId, file);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UploadApi#putUploadSessionFile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contentType** | **String**|  | [default to application/json]
 **uploadSessionId** | [**UUID**](.md)|  |
 **file** | **File**|  | [optional]

### Return type

[**InlineResponse20010**](InlineResponse20010.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

