# InlineResponse20012Rating

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**average** | [**BigDecimal**](BigDecimal.md) | Will be nullable if no ratings has been given |  [optional]
**bayesian** | [**BigDecimal**](BigDecimal.md) | Average weighted on all the Manga population |  [optional]
**distribution** | [**InlineResponse20012RatingDistribution**](InlineResponse20012RatingDistribution.md) |  |  [optional]
