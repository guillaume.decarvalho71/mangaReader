# Order6

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | [**CreatedAtEnum**](#CreatedAtEnum) |  |  [optional]
**updatedAt** | [**UpdatedAtEnum**](#UpdatedAtEnum) |  |  [optional]
**volume** | [**VolumeEnum**](#VolumeEnum) |  |  [optional]

<a name="CreatedAtEnum"></a>
## Enum: CreatedAtEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="UpdatedAtEnum"></a>
## Enum: UpdatedAtEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;

<a name="VolumeEnum"></a>
## Enum: VolumeEnum
Name | Value
---- | -----
ASC | &quot;asc&quot;
DESC | &quot;desc&quot;
