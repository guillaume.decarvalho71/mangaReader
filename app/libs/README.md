# swagger-java-client

MangaDex API
- API version: 5.7.5
  - Build date: 2022-11-08T11:09:21.386600200+01:00[Europe/Paris]

MangaDex is an ad-free manga reader offering high-quality images!  This document details our API as it is right now. It is in no way a promise to never change it, although we will endeavour to publicly notify any major change.  # Acceptable use policy  Usage of our services implies acceptance of the following: - You **MUST** credit us - You **MUST** credit scanlation groups if you offer the ability to read chapters - You **CANNOT** run ads or paid services on your website and/or apps  These may change at any time for any and no reason and it is up to you check for updates from time to time.  # Security issues  If you believe you found a security issue in our API, please check our [security.txt](/security.txt) to get in touch privately.


*Automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen)*


## Requirements

Building the API client library requires:
1. Java 1.7+
2. Maven/Gradle

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn clean install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn clean deploy
```

Refer to the [OSSRH Guide](http://central.sonatype.org/pages/ossrh-guide.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
  <groupId>io.swagger</groupId>
  <artifactId>swagger-java-client</artifactId>
  <version>1.0.0</version>
  <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "io.swagger:swagger-java-client:1.0.0"
```

### Others

At first generate the JAR by executing:

```shell
mvn clean package
```

Then manually install the following JARs:

* `target/swagger-java-client-1.0.0.jar`
* `target/lib/*.jar`

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java
package io.swagger.client;

import io.swagger.client.api.MangaApi;
import io.swagger.client.model.MangaList;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

public class test {
    public static void main(String[] args) throws IOException {
       MangaApi mangaApi=  new ApiClient().createService(MangaApi.class);
         //mangaApi;
        Call<MangaList> searchManga = mangaApi.getSearchManga(10, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        Response<MangaList> response=  searchManga.execute();

        System.out.println( response.body().toString());
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://api.mangadex.org*

| Class                  | Method                                                                                      | HTTP request                                               | Description                                             |
|------------------------|---------------------------------------------------------------------------------------------|------------------------------------------------------------|---------------------------------------------------------|
| *AccountApi*           | [**getAccountActivateCode**](docs/AccountApi.md#getAccountActivateCode)                     | **POST** /account/activate/{code}                          | Activate account                                        |
| *AccountApi*           | [**getAccountAvailable**](docs/AccountApi.md#getAccountAvailable)                           | **GET** /account/available                                 | Account username available                              |
| *AccountApi*           | [**postAccountActivateResend**](docs/AccountApi.md#postAccountActivateResend)               | **POST** /account/activate/resend                          | Resend Activation code                                  |
| *AccountApi*           | [**postAccountCreate**](docs/AccountApi.md#postAccountCreate)                               | **POST** /account/create                                   | Create Account                                          |
| *AccountApi*           | [**postAccountRecover**](docs/AccountApi.md#postAccountRecover)                             | **POST** /account/recover                                  | Recover given Account                                   |
| *AccountApi*           | [**postAccountRecoverCode**](docs/AccountApi.md#postAccountRecoverCode)                     | **POST** /account/recover/{code}                           | Complete Account recover                                |
| *AtHomeApi*            | [**getAtHomeServerChapterId**](docs/AtHomeApi.md#getAtHomeServerChapterId)                  | **GET** /at-home/server/{chapterId}                        | Get MangaDex@Home server URL                            |
| *AuthApi*              | [**getAuthCheck**](docs/AuthApi.md#getAuthCheck)                                            | **GET** /auth/check                                        | Check token                                             |
| *AuthApi*              | [**postAuthLogin**](docs/AuthApi.md#postAuthLogin)                                          | **POST** /auth/login                                       | Login                                                   |
| *AuthApi*              | [**postAuthLogout**](docs/AuthApi.md#postAuthLogout)                                        | **POST** /auth/logout                                      | Logout                                                  |
| *AuthApi*              | [**postAuthRefresh**](docs/AuthApi.md#postAuthRefresh)                                      | **POST** /auth/refresh                                     | Refresh token                                           |
| *AuthorApi*            | [**deleteAuthorId**](docs/AuthorApi.md#deleteAuthorId)                                      | **DELETE** /author/{id}                                    | Delete Author                                           |
| *AuthorApi*            | [**getAuthor**](docs/AuthorApi.md#getAuthor)                                                | **GET** /author                                            | Author list                                             |
| *AuthorApi*            | [**getAuthorId**](docs/AuthorApi.md#getAuthorId)                                            | **GET** /author/{id}                                       | Get Author                                              |
| *AuthorApi*            | [**postAuthor**](docs/AuthorApi.md#postAuthor)                                              | **POST** /author                                           | Create Author                                           |
| *AuthorApi*            | [**putAuthorId**](docs/AuthorApi.md#putAuthorId)                                            | **PUT** /author/{id}                                       | Update Author                                           |
| *CaptchaApi*           | [**postCaptchaSolve**](docs/CaptchaApi.md#postCaptchaSolve)                                 | **POST** /captcha/solve                                    | Solve Captcha                                           |
| *ChapterApi*           | [**deleteChapterId**](docs/ChapterApi.md#deleteChapterId)                                   | **DELETE** /chapter/{id}                                   | Delete Chapter                                          |
| *ChapterApi*           | [**getChapter**](docs/ChapterApi.md#getChapter)                                             | **GET** /chapter                                           | Chapter list                                            |
| *ChapterApi*           | [**getChapterId**](docs/ChapterApi.md#getChapterId)                                         | **GET** /chapter/{id}                                      | Get Chapter                                             |
| *ChapterApi*           | [**putChapterId**](docs/ChapterApi.md#putChapterId)                                         | **PUT** /chapter/{id}                                      | Update Chapter                                          |
| *ChapterReadMarkerApi* | [**getMangaChapterReadmarkers**](docs/ChapterReadMarkerApi.md#getMangaChapterReadmarkers)   | **GET** /manga/{id}/read                                   | Manga read markers                                      |
| *ChapterReadMarkerApi* | [**getMangaChapterReadmarkers2**](docs/ChapterReadMarkerApi.md#getMangaChapterReadmarkers2) | **GET** /manga/read                                        | Manga read markers                                      |
| *ChapterReadMarkerApi* | [**getReadingHistory**](docs/ChapterReadMarkerApi.md#getReadingHistory)                     | **GET** /user/history                                      | Get users reading history                               |
| *ChapterReadMarkerApi* | [**postMangaChapterReadmarkers**](docs/ChapterReadMarkerApi.md#postMangaChapterReadmarkers) | **POST** /manga/{id}/read                                  | Manga read markers batch                                |
| *CoverApi*             | [**deleteCover**](docs/CoverApi.md#deleteCover)                                             | **DELETE** /cover/{mangaOrCoverId}                         | Delete Cover                                            |
| *CoverApi*             | [**editCover**](docs/CoverApi.md#editCover)                                                 | **PUT** /cover/{mangaOrCoverId}                            | Edit Cover                                              |
| *CoverApi*             | [**getCover**](docs/CoverApi.md#getCover)                                                   | **GET** /cover                                             | CoverArt list                                           |
| *CoverApi*             | [**getCoverId**](docs/CoverApi.md#getCoverId)                                               | **GET** /cover/{mangaOrCoverId}                            | Get Cover                                               |
| *CoverApi*             | [**uploadCover**](docs/CoverApi.md#uploadCover)                                             | **POST** /cover/{mangaOrCoverId}                           | Upload Cover                                            |
| *CustomListApi*        | [**deleteListId**](docs/CustomListApi.md#deleteListId)                                      | **DELETE** /list/{id}                                      | Delete CustomList                                       |
| *CustomListApi*        | [**deleteMangaIdListListId**](docs/CustomListApi.md#deleteMangaIdListListId)                | **DELETE** /manga/{id}/list/{listId}                       | Remove Manga in CustomList                              |
| *CustomListApi*        | [**followListId**](docs/CustomListApi.md#followListId)                                      | **POST** /list/{id}/follow                                 | Follow CustomList                                       |
| *CustomListApi*        | [**getListId**](docs/CustomListApi.md#getListId)                                            | **GET** /list/{id}                                         | Get CustomList                                          |
| *CustomListApi*        | [**getUserIdList**](docs/CustomListApi.md#getUserIdList)                                    | **GET** /user/{id}/list                                    | Get User&#x27;s CustomList list                         |
| *CustomListApi*        | [**getUserList**](docs/CustomListApi.md#getUserList)                                        | **GET** /user/list                                         | Get logged User CustomList list                         |
| *CustomListApi*        | [**postList**](docs/CustomListApi.md#postList)                                              | **POST** /list                                             | Create CustomList                                       |
| *CustomListApi*        | [**postMangaIdListListId**](docs/CustomListApi.md#postMangaIdListListId)                    | **POST** /manga/{id}/list/{listId}                         | Add Manga in CustomList                                 |
| *CustomListApi*        | [**putListId**](docs/CustomListApi.md#putListId)                                            | **PUT** /list/{id}                                         | Update CustomList                                       |
| *CustomListApi*        | [**unfollowListId**](docs/CustomListApi.md#unfollowListId)                                  | **DELETE** /list/{id}/follow                               | Unfollow CustomList                                     |
| *FeedApi*              | [**getListIdFeed**](docs/FeedApi.md#getListIdFeed)                                          | **GET** /list/{id}/feed                                    | CustomList Manga feed                                   |
| *FeedApi*              | [**getUserFollowsMangaFeed**](docs/FeedApi.md#getUserFollowsMangaFeed)                      | **GET** /user/follows/manga/feed                           | Get logged User followed Manga feed (Chapter list)      |
| *FollowsApi*           | [**getUserFollowsGroup**](docs/FollowsApi.md#getUserFollowsGroup)                           | **GET** /user/follows/group                                | Get logged User followed Groups                         |
| *FollowsApi*           | [**getUserFollowsGroupId**](docs/FollowsApi.md#getUserFollowsGroupId)                       | **GET** /user/follows/group/{id}                           | Check if logged User follows a Group                    |
| *FollowsApi*           | [**getUserFollowsList**](docs/FollowsApi.md#getUserFollowsList)                             | **GET** /user/follows/list                                 | Get logged User followed CustomList list                |
| *FollowsApi*           | [**getUserFollowsListId**](docs/FollowsApi.md#getUserFollowsListId)                         | **GET** /user/follows/list/{id}                            | Check if logged User follows a CustomList               |
| *FollowsApi*           | [**getUserFollowsManga**](docs/FollowsApi.md#getUserFollowsManga)                           | **GET** /user/follows/manga                                | Get logged User followed Manga list                     |
| *FollowsApi*           | [**getUserFollowsMangaId**](docs/FollowsApi.md#getUserFollowsMangaId)                       | **GET** /user/follows/manga/{id}                           | Check if logged User follows a Manga                    |
| *FollowsApi*           | [**getUserFollowsUser**](docs/FollowsApi.md#getUserFollowsUser)                             | **GET** /user/follows/user                                 | Get logged User followed User list                      |
| *FollowsApi*           | [**getUserFollowsUserId**](docs/FollowsApi.md#getUserFollowsUserId)                         | **GET** /user/follows/user/{id}                            | Check if logged User follows a User                     |
| *InfrastructureApi*    | [**pingGet**](docs/InfrastructureApi.md#pingGet)                                            | **GET** /ping                                              | Ping the server                                         |
| *LegacyApi*            | [**postLegacyMapping**](docs/LegacyApi.md#postLegacyMapping)                                | **POST** /legacy/mapping                                   | Legacy ID mapping                                       |
| *MangaApi*             | [**commitMangaDraft**](docs/MangaApi.md#commitMangaDraft)                                   | **POST** /manga/draft/{id}/commit                          | Submit a Manga Draft                                    |
| *MangaApi*             | [**deleteMangaId**](docs/MangaApi.md#deleteMangaId)                                         | **DELETE** /manga/{id}                                     | Delete Manga                                            |
| *MangaApi*             | [**deleteMangaIdFollow**](docs/MangaApi.md#deleteMangaIdFollow)                             | **DELETE** /manga/{id}/follow                              | Unfollow Manga                                          |
| *MangaApi*             | [**deleteMangaRelationId**](docs/MangaApi.md#deleteMangaRelationId)                         | **DELETE** /manga/{mangaId}/relation/{id}                  | Delete Manga relation                                   |
| *MangaApi*             | [**getMangaDrafts**](docs/MangaApi.md#getMangaDrafts)                                       | **GET** /manga/draft                                       | Get a list of Manga Drafts                              |
| *MangaApi*             | [**getMangaId**](docs/MangaApi.md#getMangaId)                                               | **GET** /manga/{id}                                        | Get Manga                                               |
| *MangaApi*             | [**getMangaIdDraft**](docs/MangaApi.md#getMangaIdDraft)                                     | **GET** /manga/draft/{id}                                  | Get a specific Manga Draft                              |
| *MangaApi*             | [**getMangaIdFeed**](docs/MangaApi.md#getMangaIdFeed)                                       | **GET** /manga/{id}/feed                                   | Manga feed                                              |
| *MangaApi*             | [**getMangaIdStatus**](docs/MangaApi.md#getMangaIdStatus)                                   | **GET** /manga/{id}/status                                 | Get a Manga reading status                              |
| *MangaApi*             | [**getMangaRandom**](docs/MangaApi.md#getMangaRandom)                                       | **GET** /manga/random                                      | Get a random Manga                                      |
| *MangaApi*             | [**getMangaRelation**](docs/MangaApi.md#getMangaRelation)                                   | **GET** /manga/{mangaId}/relation                          | Manga relation list                                     |
| *MangaApi*             | [**getMangaStatus**](docs/MangaApi.md#getMangaStatus)                                       | **GET** /manga/status                                      | Get all Manga reading status for logged User            |
| *MangaApi*             | [**getMangaTag**](docs/MangaApi.md#getMangaTag)                                             | **GET** /manga/tag                                         | Tag list                                                |
| *MangaApi*             | [**getSearchManga**](docs/MangaApi.md#getSearchManga)                                       | **GET** /manga                                             | Manga list                                              |
| *MangaApi*             | [**mangaIdAggregateGet**](docs/MangaApi.md#mangaIdAggregateGet)                             | **GET** /manga/{id}/aggregate                              | Get Manga volumes &amp; chapters                        |
| *MangaApi*             | [**postManga**](docs/MangaApi.md#postManga)                                                 | **POST** /manga                                            | Create Manga                                            |
| *MangaApi*             | [**postMangaIdFollow**](docs/MangaApi.md#postMangaIdFollow)                                 | **POST** /manga/{id}/follow                                | Follow Manga                                            |
| *MangaApi*             | [**postMangaIdStatus**](docs/MangaApi.md#postMangaIdStatus)                                 | **POST** /manga/{id}/status                                | Update Manga reading status                             |
| *MangaApi*             | [**postMangaRelation**](docs/MangaApi.md#postMangaRelation)                                 | **POST** /manga/{mangaId}/relation                         | Create Manga relation                                   |
| *MangaApi*             | [**putMangaId**](docs/MangaApi.md#putMangaId)                                               | **PUT** /manga/{id}                                        | Update Manga                                            |
| *RatingApi*            | [**deleteRatingMangaId**](docs/RatingApi.md#deleteRatingMangaId)                            | **DELETE** /rating/{mangaId}                               | Delete Manga rating                                     |
| *RatingApi*            | [**getRating**](docs/RatingApi.md#getRating)                                                | **GET** /rating                                            | Get your ratings                                        |
| *RatingApi*            | [**postRatingMangaId**](docs/RatingApi.md#postRatingMangaId)                                | **POST** /rating/{mangaId}                                 | Create or update Manga rating                           |
| *ReportApi*            | [**getReportReasonsByCategory**](docs/ReportApi.md#getReportReasonsByCategory)              | **GET** /report/reasons/{category}                         | Get a list of report reasons                            |
| *ReportApi*            | [**getReports**](docs/ReportApi.md#getReports)                                              | **GET** /report                                            | Get a list of reports by the user                       |
| *ReportApi*            | [**postReport**](docs/ReportApi.md#postReport)                                              | **POST** /report                                           | Create a new Report                                     |
| *ScanlationGroupApi*   | [**deleteGroupId**](docs/ScanlationGroupApi.md#deleteGroupId)                               | **DELETE** /group/{id}                                     | Delete Scanlation Group                                 |
| *ScanlationGroupApi*   | [**deleteGroupIdFollow**](docs/ScanlationGroupApi.md#deleteGroupIdFollow)                   | **DELETE** /group/{id}/follow                              | Unfollow Scanlation Group                               |
| *ScanlationGroupApi*   | [**getGroupId**](docs/ScanlationGroupApi.md#getGroupId)                                     | **GET** /group/{id}                                        | Get Scanlation Group                                    |
| *ScanlationGroupApi*   | [**getSearchGroup**](docs/ScanlationGroupApi.md#getSearchGroup)                             | **GET** /group                                             | Scanlation Group list                                   |
| *ScanlationGroupApi*   | [**postGroup**](docs/ScanlationGroupApi.md#postGroup)                                       | **POST** /group                                            | Create Scanlation Group                                 |
| *ScanlationGroupApi*   | [**postGroupIdFollow**](docs/ScanlationGroupApi.md#postGroupIdFollow)                       | **POST** /group/{id}/follow                                | Follow Scanlation Group                                 |
| *ScanlationGroupApi*   | [**putGroupId**](docs/ScanlationGroupApi.md#putGroupId)                                     | **PUT** /group/{id}                                        | Update Scanlation Group                                 |
| *SettingsApi*          | [**getSettings**](docs/SettingsApi.md#getSettings)                                          | **GET** /settings                                          | Get an User Settings                                    |
| *SettingsApi*          | [**getSettingsTemplate**](docs/SettingsApi.md#getSettingsTemplate)                          | **GET** /settings/template                                 | Get latest Settings template                            |
| *SettingsApi*          | [**getSettingsTemplateVersion**](docs/SettingsApi.md#getSettingsTemplateVersion)            | **GET** /settings/template/{version}                       | Get Settings template by version id                     |
| *SettingsApi*          | [**postSettings**](docs/SettingsApi.md#postSettings)                                        | **POST** /settings                                         | Create or update an User Settings                       |
| *SettingsApi*          | [**postSettingsTemplate**](docs/SettingsApi.md#postSettingsTemplate)                        | **POST** /settings/template                                | Create Settings template                                |
| *StatisticsApi*        | [**getStatisticsManga**](docs/StatisticsApi.md#getStatisticsManga)                          | **GET** /statistics/manga                                  | Find statistics about given Manga                       |
| *StatisticsApi*        | [**getStatisticsMangaUuid**](docs/StatisticsApi.md#getStatisticsMangaUuid)                  | **GET** /statistics/manga/{uuid}                           | Get statistics about given Manga                        |
| *UploadApi*            | [**abandonUploadSession**](docs/UploadApi.md#abandonUploadSession)                          | **DELETE** /upload/{uploadSessionId}                       | Abandon upload session                                  |
| *UploadApi*            | [**beginEditSession**](docs/UploadApi.md#beginEditSession)                                  | **POST** /upload/begin/{chapterId}                         | Start an edit chapter session                           |
| *UploadApi*            | [**beginUploadSession**](docs/UploadApi.md#beginUploadSession)                              | **POST** /upload/begin                                     | Start an upload session                                 |
| *UploadApi*            | [**commitUploadSession**](docs/UploadApi.md#commitUploadSession)                            | **POST** /upload/{uploadSessionId}/commit                  | Commit the upload session and specify chapter data      |
| *UploadApi*            | [**deleteUploadedSessionFile**](docs/UploadApi.md#deleteUploadedSessionFile)                | **DELETE** /upload/{uploadSessionId}/{uploadSessionFileId} | Delete an uploaded image from the Upload Session        |
| *UploadApi*            | [**deleteUploadedSessionFiles**](docs/UploadApi.md#deleteUploadedSessionFiles)              | **DELETE** /upload/{uploadSessionId}/batch                 | Delete a set of uploaded images from the Upload Session |
| *UploadApi*            | [**getUploadSession**](docs/UploadApi.md#getUploadSession)                                  | **GET** /upload                                            | Get the current User upload session                     |
| *UploadApi*            | [**putUploadSessionFile**](docs/UploadApi.md#putUploadSessionFile)                          | **POST** /upload/{uploadSessionId}                         | Upload images to the upload session                     |
| *UserApi*              | [**deleteUserId**](docs/UserApi.md#deleteUserId)                                            | **DELETE** /user/{id}                                      | Delete User                                             |
| *UserApi*              | [**getUser**](docs/UserApi.md#getUser)                                                      | **GET** /user                                              | User list                                               |
| *UserApi*              | [**getUserId**](docs/UserApi.md#getUserId)                                                  | **GET** /user/{id}                                         | Get User                                                |
| *UserApi*              | [**getUserMe**](docs/UserApi.md#getUserMe)                                                  | **GET** /user/me                                           | Logged User details                                     |
| *UserApi*              | [**postUserDeleteCode**](docs/UserApi.md#postUserDeleteCode)                                | **POST** /user/delete/{code}                               | Approve User deletion                                   |
| *UserApi*              | [**postUserEmail**](docs/UserApi.md#postUserEmail)                                          | **POST** /user/email                                       | Update User email                                       |
| *UserApi*              | [**postUserPassword**](docs/UserApi.md#postUserPassword)                                    | **POST** /user/password                                    | Update User password                                    |

## Documentation for Models

 - [AccountActivateResponse](docs/AccountActivateResponse.md)
 - [AnyOfChapterReadMarkerBatch](docs/AnyOfChapterReadMarkerBatch.md)
 - [Author](docs/Author.md)
 - [AuthorAttributes](docs/AuthorAttributes.md)
 - [AuthorCreate](docs/AuthorCreate.md)
 - [AuthorEdit](docs/AuthorEdit.md)
 - [AuthorList](docs/AuthorList.md)
 - [AuthorResponse](docs/AuthorResponse.md)
 - [BeginEditSession](docs/BeginEditSession.md)
 - [BeginUploadSession](docs/BeginUploadSession.md)
 - [CaptchaSolveBody](docs/CaptchaSolveBody.md)
 - [Chapter](docs/Chapter.md)
 - [ChapterAttributes](docs/ChapterAttributes.md)
 - [ChapterDraft](docs/ChapterDraft.md)
 - [ChapterEdit](docs/ChapterEdit.md)
 - [ChapterList](docs/ChapterList.md)
 - [ChapterReadMarkerBatch](docs/ChapterReadMarkerBatch.md)
 - [ChapterRequest](docs/ChapterRequest.md)
 - [ChapterResponse](docs/ChapterResponse.md)
 - [CheckResponse](docs/CheckResponse.md)
 - [CommitUploadSession](docs/CommitUploadSession.md)
 - [Cover](docs/Cover.md)
 - [CoverAttributes](docs/CoverAttributes.md)
 - [CoverEdit](docs/CoverEdit.md)
 - [CoverList](docs/CoverList.md)
 - [CoverMangaOrCoverIdBody](docs/CoverMangaOrCoverIdBody.md)
 - [CoverResponse](docs/CoverResponse.md)
 - [CreateAccount](docs/CreateAccount.md)
 - [CreateScanlationGroup](docs/CreateScanlationGroup.md)
 - [CustomList](docs/CustomList.md)
 - [CustomListAttributes](docs/CustomListAttributes.md)
 - [CustomListCreate](docs/CustomListCreate.md)
 - [CustomListEdit](docs/CustomListEdit.md)
 - [CustomListList](docs/CustomListList.md)
 - [CustomListResponse](docs/CustomListResponse.md)
 - [Error](docs/Error.md)
 - [ErrorResponse](docs/ErrorResponse.md)
 - [IdCommitBody](docs/IdCommitBody.md)
 - [InlineResponse200](docs/InlineResponse200.md)
 - [InlineResponse2001](docs/InlineResponse2001.md)
 - [InlineResponse20010](docs/InlineResponse20010.md)
 - [InlineResponse20011](docs/InlineResponse20011.md)
 - [InlineResponse20011Ratings](docs/InlineResponse20011Ratings.md)
 - [InlineResponse20012](docs/InlineResponse20012.md)
 - [InlineResponse20012Rating](docs/InlineResponse20012Rating.md)
 - [InlineResponse20012RatingDistribution](docs/InlineResponse20012RatingDistribution.md)
 - [InlineResponse20012Statistics](docs/InlineResponse20012Statistics.md)
 - [InlineResponse20013](docs/InlineResponse20013.md)
 - [InlineResponse20013Rating](docs/InlineResponse20013Rating.md)
 - [InlineResponse20013Statistics](docs/InlineResponse20013Statistics.md)
 - [InlineResponse20014](docs/InlineResponse20014.md)
 - [InlineResponse20015](docs/InlineResponse20015.md)
 - [InlineResponse20016](docs/InlineResponse20016.md)
 - [InlineResponse20016Ratings](docs/InlineResponse20016Ratings.md)
 - [InlineResponse2002](docs/InlineResponse2002.md)
 - [InlineResponse2003](docs/InlineResponse2003.md)
 - [InlineResponse2004](docs/InlineResponse2004.md)
 - [InlineResponse2005](docs/InlineResponse2005.md)
 - [InlineResponse2005Chapter](docs/InlineResponse2005Chapter.md)
 - [InlineResponse2006](docs/InlineResponse2006.md)
 - [InlineResponse2007](docs/InlineResponse2007.md)
 - [InlineResponse2008](docs/InlineResponse2008.md)
 - [InlineResponse2009](docs/InlineResponse2009.md)
 - [InlineResponse2009Attributes](docs/InlineResponse2009Attributes.md)
 - [InlineResponse2009Data](docs/InlineResponse2009Data.md)
 - [InlineResponse200Chapters](docs/InlineResponse200Chapters.md)
 - [InlineResponse200Volumes](docs/InlineResponse200Volumes.md)
 - [LocalizedString](docs/LocalizedString.md)
 - [Login](docs/Login.md)
 - [LoginResponse](docs/LoginResponse.md)
 - [LoginResponseToken](docs/LoginResponseToken.md)
 - [LogoutResponse](docs/LogoutResponse.md)
 - [Manga](docs/Manga.md)
 - [MangaAttributes](docs/MangaAttributes.md)
 - [MangaCreate](docs/MangaCreate.md)
 - [MangaEdit](docs/MangaEdit.md)
 - [MangaIdBody](docs/MangaIdBody.md)
 - [MangaList](docs/MangaList.md)
 - [MangaRelation](docs/MangaRelation.md)
 - [MangaRelationAttributes](docs/MangaRelationAttributes.md)
 - [MangaRelationCreate](docs/MangaRelationCreate.md)
 - [MangaRelationList](docs/MangaRelationList.md)
 - [MangaRelationRequest](docs/MangaRelationRequest.md)
 - [MangaRelationResponse](docs/MangaRelationResponse.md)
 - [MangaRequest](docs/MangaRequest.md)
 - [MangaResponse](docs/MangaResponse.md)
 - [MappingId](docs/MappingId.md)
 - [MappingIdAttributes](docs/MappingIdAttributes.md)
 - [MappingIdBody](docs/MappingIdBody.md)
 - [MappingIdResponse](docs/MappingIdResponse.md)
 - [OneOfinlineResponse2004Data](docs/OneOfinlineResponse2004Data.md)
 - [Order](docs/Order.md)
 - [Order1](docs/Order1.md)
 - [Order10](docs/Order10.md)
 - [Order2](docs/Order2.md)
 - [Order3](docs/Order3.md)
 - [Order4](docs/Order4.md)
 - [Order5](docs/Order5.md)
 - [Order6](docs/Order6.md)
 - [Order7](docs/Order7.md)
 - [Order8](docs/Order8.md)
 - [Order9](docs/Order9.md)
 - [RatingMangaIdBody](docs/RatingMangaIdBody.md)
 - [RecoverCompleteBody](docs/RecoverCompleteBody.md)
 - [ReferenceExpansion](docs/ReferenceExpansion.md)
 - [RefreshResponse](docs/RefreshResponse.md)
 - [RefreshToken](docs/RefreshToken.md)
 - [Relationship](docs/Relationship.md)
 - [Report](docs/Report.md)
 - [ReportAttributes](docs/ReportAttributes.md)
 - [ReportBody](docs/ReportBody.md)
 - [ReportListResponse](docs/ReportListResponse.md)
 - [Response](docs/Response.md)
 - [ScanlationGroup](docs/ScanlationGroup.md)
 - [ScanlationGroupAttributes](docs/ScanlationGroupAttributes.md)
 - [ScanlationGroupEdit](docs/ScanlationGroupEdit.md)
 - [ScanlationGroupList](docs/ScanlationGroupList.md)
 - [ScanlationGroupResponse](docs/ScanlationGroupResponse.md)
 - [SendAccountActivationCode](docs/SendAccountActivationCode.md)
 - [SettingsBody](docs/SettingsBody.md)
 - [Tag](docs/Tag.md)
 - [TagAttributes](docs/TagAttributes.md)
 - [TagResponse](docs/TagResponse.md)
 - [UpdateMangaStatus](docs/UpdateMangaStatus.md)
 - [UploadSession](docs/UploadSession.md)
 - [UploadSessionAttributes](docs/UploadSessionAttributes.md)
 - [UploadSessionFile](docs/UploadSessionFile.md)
 - [UploadSessionFileAttributes](docs/UploadSessionFileAttributes.md)
 - [UploadUploadSessionIdBody](docs/UploadUploadSessionIdBody.md)
 - [Uploader](docs/Uploader.md)
 - [User](docs/User.md)
 - [UserAttributes](docs/UserAttributes.md)
 - [UserEmailBody](docs/UserEmailBody.md)
 - [UserList](docs/UserList.md)
 - [UserPasswordBody](docs/UserPasswordBody.md)
 - [UserResponse](docs/UserResponse.md)
 - [Volume](docs/Volume.md)

## Documentation for Authorization

Authentication schemes defined for the API:
### Bearer



## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author

support@mangadex.org
