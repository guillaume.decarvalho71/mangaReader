package com.perso.mangareader.comparator;

import java.util.Comparator;

import io.swagger.client.model.Relationship;

public class ComparatorRelationship implements Comparator<Relationship> {
    @Override
    public int compare(Relationship relationship, Relationship t1) {
        String relationshipType = relationship.getType();
        String t1Type = t1.getType();
        if(relationshipType.equals(t1Type))
            return 0;
        else
            return -1;
    }
}
