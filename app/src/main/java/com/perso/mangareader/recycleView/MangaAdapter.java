package com.perso.mangareader.recycleView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.perso.mangareader.R;

import java.util.List;

import io.swagger.client.ApiClient;
import io.swagger.client.api.MangaApi;
import io.swagger.client.model.Manga;
import io.swagger.client.model.MangaList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MangaAdapter extends RecyclerView.Adapter<MangaListViewHolder> {
    List<Manga> mangaList;

    public MangaAdapter(List<Manga> mangaList) {
        this.mangaList = mangaList;
    }

    @NonNull
    @Override
    public MangaListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.fragment_main_item, parent, false);


        return new MangaListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MangaListViewHolder holder, int position) {
        holder.updateWithManga(this.mangaList.get(position));
        int size = this.mangaList.size();
        if (position + 1 == size && (position + 1)%10==0) {
            MangaApi mangaApi = new ApiClient().createService(MangaApi.class);
            Call<MangaList> searchManga = mangaApi.getSearchManga(10, size, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, "asc", null, null, null, null, null, null, null, null, null, null);
            searchManga.enqueue(new Callback<MangaList>() {
                @Override
                public void onResponse(@NonNull Call<MangaList> call, @NonNull Response<MangaList> response) {

                    if (response.body() != null) {
                        mangaList.addAll(response.body().getData());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MangaList> call, @NonNull Throwable throwable) {
                    call.cancel();
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return this.mangaList.size();
    }
}
