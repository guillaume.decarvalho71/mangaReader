package com.perso.mangareader.recycleView;


import static com.perso.mangareader.api.adapter.CoverApiAdapter.getLastCover;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.perso.mangareader.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.swagger.client.model.Cover;
import io.swagger.client.model.Manga;

public class MangaListViewHolder extends RecyclerView.ViewHolder {
    private View view;

    @BindView(R.id.fragment_main_item_title)
    TextView textView;

    @BindView(R.id.imageView)
    ImageView imageView;
    private static final String BASE_URL = "https://uploads.mangadex.org/covers/";

    public MangaListViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);


    }

    public void updateWithManga(Manga manga) {


        String en = manga.getAttributes().getTitle().get("en");

        this.textView.setText(en);
        view = textView.getRootView();
        downloadLastCoverManga(manga);


    }


    public void downloadLastCoverManga(Manga manga) {
        getLastCover(manga, view, imageView);
    }

    @NonNull
    private String buildLinkString(List<Cover> data, Manga manga) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(BASE_URL);
        stringBuilder.append(manga.getId());
        stringBuilder.append("/");
        Cover cover1 = data.get(0);
        stringBuilder.append(cover1.getAttributes().getFileName());
        return stringBuilder.toString();
    }


}

