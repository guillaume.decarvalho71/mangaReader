package com.perso.mangareader;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.perso.mangareader.recycleView.MangaAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.swagger.client.ApiClient;
import io.swagger.client.api.MangaApi;
import io.swagger.client.model.Manga;
import io.swagger.client.model.MangaList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainFragment extends Fragment {
    // FOR DESIGN
    @BindView(R.id.fragment_main_recycler_view)
    RecyclerView recyclerView;


    private List<Manga> mangaList;
    private MangaAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);
        this.configureRecyclerView();
        executeHttpRequestWithRetrofit();
        return view;
    }



    // -----------------
    // CONFIGURATION
    // -----------------

    private void configureRecyclerView() {
        mangaList = new ArrayList<>();
        // Create adapter passing in the sample user data
        this.adapter = new MangaAdapter(this.mangaList);
        // Attach the adapter to the recyclerview to populate items
        this.recyclerView.setAdapter(this.adapter);
        // Set layout manager to position the items
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    // -------------------
    // HTTP (RxJAVA)
    // -------------------

    private void executeHttpRequestWithRetrofit() {
        MangaApi mangaApi = new ApiClient().createService(MangaApi.class);
        Call<MangaList> searchManga = mangaApi.getSearchManga(10, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, "asc", null, null, null, null, null, null, null, null, null, null);
        searchManga.enqueue(new Callback<MangaList>() {
            @Override
            public void onResponse(@NonNull Call<MangaList> call, @NonNull Response<MangaList> response) {

                if (response.body() != null) {
                    updateUI(response.body());
                }
            }

            @Override
            public void onFailure(@NonNull Call<MangaList> call, @NonNull Throwable throwable) {
                call.cancel();
            }
        });
    }


    // -------------------
    // UPDATE UI
    // -------------------

    private void updateUI(MangaList mangaList) {
        this.mangaList.addAll(mangaList.getData());
        adapter.notifyDataSetChanged();
    }
}
