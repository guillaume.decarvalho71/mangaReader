package com.perso.mangareader.api.adapter;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import io.swagger.client.ApiClient;
import io.swagger.client.api.CoverApi;
import io.swagger.client.model.Cover;
import io.swagger.client.model.CoverList;
import io.swagger.client.model.Manga;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CoverApiAdapter {

    private CoverApi coverApi;
    private UUID mangasId;
    private static final String BASE_URL = "https://uploads.mangadex.org/covers/";


    public CoverApiAdapter(UUID mangaId) {
        coverApi = new ApiClient().createService(CoverApi.class);
        this.mangasId = mangaId;
    }

    private Call<CoverList> getLastCoverRequest() {
        List<UUID> uuidList = Collections.singletonList(mangasId);
        return coverApi.getCover(1, null, uuidList, null, null, null, null, null, "desc", null);
    }

    public static void getLastCover(Manga manga, View view, ImageView imageView) {
        CoverApiAdapter coverApiAdapter = new CoverApiAdapter(manga.getId());
        Call<CoverList> cover = coverApiAdapter.getLastCoverRequest();

        cover.enqueue(new Callback<CoverList>() {
            @Override
            public void onResponse(Call<CoverList> call, Response<CoverList> response) {
                CoverList body = response.body();
                List<Cover> data = body.getData();
                String link = buildLinkString(data, manga);
                Glide.with(view).load(link).into(imageView);
            }

            @Override
            public void onFailure(Call<CoverList> call, Throwable throwable) {
                call.cancel();
            }
        });
    }


    @NonNull
    private static String buildLinkString(List<Cover> data, Manga manga) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(BASE_URL);
        stringBuilder.append(manga.getId());
        stringBuilder.append("/");
        Cover cover1 = data.get(0);
        stringBuilder.append(cover1.getAttributes().getFileName());
        return stringBuilder.toString();
    }

}
