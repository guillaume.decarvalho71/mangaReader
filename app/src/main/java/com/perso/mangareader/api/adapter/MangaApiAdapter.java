package com.perso.mangareader.api.adapter;

import java.util.Collections;
import java.util.List;

import io.swagger.client.ApiClient;
import io.swagger.client.api.MangaApi;
import io.swagger.client.model.MangaList;
import retrofit2.Call;

public class MangaApiAdapter {
    private static final List<String> ORIGINAL_LANGUAGE = Collections.singletonList("ja");
    MangaApi mangaApi;
    private List<String> researchLanguage;
    public MangaApiAdapter(List<String> researchLanguage){
        mangaApi=new  ApiClient().createService(MangaApi.class);
        this.researchLanguage.addAll(researchLanguage);
    }
    public Call<MangaList> researchMangaByNameOrderByAsc(String  MangasTitle ){
        return mangaApi.getSearchManga(10,null,MangasTitle,null,null,
                null,null,null,null,null,
                null,null, ORIGINAL_LANGUAGE,null,
                researchLanguage,null,null,null,
                null,null,"ASC",null,null,
                null,null,null,null,
                null,null,null,null);
    }
    public Call<MangaList> researchMangaByNameOrderByDesc(String  MangasTitle ){
        return mangaApi.getSearchManga(10,null,MangasTitle,null,null,
                null,null,null,null,null,
                null,null, ORIGINAL_LANGUAGE,null,
                researchLanguage,null,null,null,
                null,null,"DESC",null,null,
                null,null,null,null,
                null,null,null,null);
    }
    public Call<MangaList> getAllMangas(int offSet ){
        return mangaApi.getSearchManga(10,offSet,null,null,null,
                null,null,null,null,null,
                null,null, ORIGINAL_LANGUAGE,null,
                researchLanguage,null,null,null,
                null,null,"ASC",null,null,
                null,null,null,null,
                null,null,null,null);
    }
}


